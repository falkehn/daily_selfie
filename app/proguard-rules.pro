# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

 -dontwarn com.yalantis.ucrop**
 -keep class com.yalantis.ucrop** { *; }
 -keep interface com.yalantis.ucrop** { *; }

 # Optimization is turned off by default. Dex does not like code run through the ProGuard optimize and preverify steps (and performs some of these
 # optimizations on its own).
 # Note that if you want to enable optimization, you cannot just include optimization flags in your own project configuration file;
 # instead you will need to point to the "proguard-android-optimize.txt" file instead of this one from your project.properties file.
 -dontoptimize
 # when eventually targeting Android, it is not necessary, so you can then switch it off to reduce the processing time a bit.
 # https://www.guardsquare.com/en/proguard/manual/usage
 -dontpreverify


 -keepnames class kotlinx.** { *; }

 # --- Kotlin begin -----------------------------------------------------------------------------------------------------------------------------------
 # https://stackoverflow.com/questions/44161717/proguard-and-kotlin-reflect-kotlin-annotations
 -dontwarn kotlin.reflect.jvm.internal.impl.descriptors.CallableDescriptor
 -dontwarn kotlin.reflect.jvm.internal.impl.descriptors.ClassDescriptor
 -dontwarn kotlin.reflect.jvm.internal.impl.descriptors.ClassifierDescriptorWithTypeParameters
 -dontwarn kotlin.reflect.jvm.internal.impl.descriptors.annotations.AnnotationDescriptor
 -dontwarn kotlin.reflect.jvm.internal.impl.descriptors.impl.PropertyDescriptorImpl
 -dontwarn kotlin.reflect.jvm.internal.impl.load.java.JavaClassFinder
 -dontwarn kotlin.reflect.jvm.internal.impl.resolve.OverridingUtil
 -dontwarn kotlin.reflect.jvm.internal.impl.types.DescriptorSubstitutor
 -dontwarn kotlin.reflect.jvm.internal.impl.types.DescriptorSubstitutor
 -dontwarn kotlin.reflect.jvm.internal.impl.types.TypeConstructor
 # needed, otherwise connectivity issue
 -keep class kotlin.** { *; }
 # --- Kotlin end -------------------------------------------------------------------------------------------------------------------------------------

 # --- Coroutines begin -------------------------------------------------------------------------------------------------------------------------------
 # ServiceLoader support
 -keepnames class kotlinx.coroutines.internal.MainDispatcherFactory {}
 -keepnames class kotlinx.coroutines.CoroutineExceptionHandler {}

 # Most of volatile fields are updated with AFU and should not be mangled
 -keepclassmembernames class kotlinx.** {
     volatile <fields>;
 }
 # --- Coroutines end ---------------------------------------------------------------------------------------------------------------------------------

 # --- Android Room (DB) begin ------------------------------------------------------------------------------------------------------------------------
 -dontwarn android.arch.util.paging.CountedDataSource
 -dontwarn androidx.room.paging.LimitOffsetDataSource
 # --- Android Room (DB) end --------------------------------------------------------------------------------------------------------------------------

 # --- Okio begin -------------------------------------------------------------------------------------------------------------------------------------
 -dontwarn okio.**
 # --- Okio end ---------------------------------------------------------------------------------------------------------------------------------------

 # --- Okhttp 3 begin ---------------------------------------------------------------------------------------------------------------------------------
 -dontwarn okhttp3.**
 -dontnote okhttp3.**
 # --- Okhttp 3 end -----------------------------------------------------------------------------------------------------------------------------------

 # --- Retrofi2 begin ---------------------------------------------------------------------------------------------------------------------------------
 # Platform calls Class.forName on types which do not exist on Android to determine platform.
 -dontnote retrofit2.Platform
 # Platform used when running on Java 8 VMs. Will not be used at runtime.
 -dontwarn retrofit2.Platform$Java8
 # Retain generic type information for use by reflection by converters and adapters.
 -keepattributes Signature
 # Retain declared checked exceptions for use by a Proxy instance.
 -keepattributes Exceptions
 -keepattributes InnerClasses
 -dontwarn javax.annotation.**
 # --- Retrifit2 end ----------------------------------------------------------------------------------------------------------------------------------

 # --- BottomBar begin --------------------------------------------------------------------------------------------------------------------------------
 -keepclassmembers public class * extends androidx.appcompat.widget.Toolbar {*;}
 # --- BottomBar end ----------------------------------------------------------------------------------------------------------------------------------

 # --- GCM Google begin -------------------------------------------------------------------------------------------------------------------------------
 # needed for Google Maps
 -dontwarn com.google.android.gms.**
 -keep class com.google.android.gms.** {*;}
 -keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
     public static final *** NULL;
 }
 -keepnames @com.google.android.gms.common.annotation.KeepName class *
 -keepclassmembernames class * {
     @com.google.android.gms.common.annotation.KeepName *;
 }
 # --- GCM Google end ---------------------------------------------------------------------------------------------------------------------------------

 # --- Enumeration classes begin ----------------------------------------------------------------------------------------------------------------------
 -keepclassmembers enum * {
     public static **[] values();
     public static ** valueOf(java.lang.String);
 }
 # --- Enumeration classes end ------------------------------------------------------------------------------------------------------------------------

 # --- Checkout Javascript Interface begin ------------------------------------------------------------------------------------------------------------
 -keepclassmembers class * {
     @android.webkit.JavascriptInterface <methods>;
 }
 # --- Enumeration Javascript Interface end -----------------------------------------------------------------------------------------------------------

 # --- Glide begin ------------------------------------------------------------------------------------------------------------------------------------
 -keep public class * implements com.bumptech.glide.module.GlideModule
 -keep public class * extends com.bumptech.glide.module.AppGlideModule
 -keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
   **[] $VALUES;
   public *;
 }
 # --- Glide end --------------------------------------------------------------------------------------------------------------------------------------

 # --- Firebase begin ---------------------------------------------------------------------------------------------------------------------------------
 -keep class com.firebase.** { *; }
 -keep class com.google.firebase.** { *; }
 -keep class org.apache.** { *; }
 -keepnames class com.fasterxml.jackson.** { *; }
 -keepnames class javax.servlet.** { *; }
 -keepnames class org.ietf.jgss.** { *; }
 -dontwarn org.apache.**
 -dontwarn org.w3c.dom.**
 -keepclassmembers class de.sieber.dailyselfie.ui.common.** { *; }

 # --- Firebase end -----------------------------------------------------------------------------------------------------------------------------------

 # --- CoreComponentFactory ---------------------------------------------------------------------------------------------------------------------------
 -keep class androidx.core.app.CoreComponentFactory { *; }
 # --- CoreComponentFactory ---------------------------------------------------------------------------------------------------------------------------
