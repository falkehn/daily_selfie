package de.sieber.dailyselfie

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.google.firebase.FirebaseApp
import de.sieber.dailyselfie.di.databaseModules
import de.sieber.dailyselfie.di.repositoryModules
import de.sieber.dailyselfie.di.utilsModules
import de.sieber.dailyselfie.di.viewModelModules
import org.koin.android.ext.android.startKoin
import timber.log.Timber


class MobileApplication() : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin(
            androidContext = this,
            modules = listOf(databaseModules, repositoryModules, utilsModules, viewModelModules)
        )
    }

}