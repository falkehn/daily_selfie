package de.sieber.dailyselfie.ui.google.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.google.photos.library.v1.PhotosLibraryClient
import de.sieber.dailyselfie.ui.model.GooglePhotoModel

class GooglePhotosDataFactory(
    private val googlePhotosLibraryClient: PhotosLibraryClient
) : DataSource.Factory<String, GooglePhotoModel>() {

    val sourceLiveData = MutableLiveData<GooglePhotosDataSource>()

    override fun create(): DataSource<String, GooglePhotoModel> {
        val dataSource = GooglePhotosDataSource(
            googlePhotosLibraryClient = googlePhotosLibraryClient
        )
        sourceLiveData.postValue(dataSource)
        return dataSource
    }

}