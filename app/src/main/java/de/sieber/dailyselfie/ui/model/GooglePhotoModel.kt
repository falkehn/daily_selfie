package de.sieber.dailyselfie.ui.model

import androidx.databinding.ObservableBoolean

data class GooglePhotoModel(
    val fileName: String = "",
    val smallImageUrl: String = "",
    val largeImageUrl: String = "",
    val creationTimeAsString: String = "",
    val isSelected: ObservableBoolean = ObservableBoolean(false),
    val creationTimeInSeconds: Long = 0,
    val id: String = "",
    val localAlbumId: Long = 0
)