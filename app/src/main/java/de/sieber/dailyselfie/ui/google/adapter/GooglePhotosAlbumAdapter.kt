package de.sieber.dailyselfie.ui.google.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.google.viewholder.GooglePhotosAlbumViewHolder
import de.sieber.dailyselfie.ui.model.GooglePhotoAlbumModel

class GooglePhotosAlbumAdapter(
    private val onAlbumClick: (googlePhotosAlbumModel: GooglePhotoAlbumModel) -> Unit
) : RecyclerView.Adapter<GooglePhotosAlbumViewHolder>() {

    private val album = mutableListOf<GooglePhotoAlbumModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GooglePhotosAlbumViewHolder {
        return GooglePhotosAlbumViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_google_photo_album, parent, false
            )
        )
    }

    override fun getItemCount(): Int = album.size

    override fun onBindViewHolder(holder: GooglePhotosAlbumViewHolder, position: Int) =
        holder.bind(album[position], onAlbumClick)

    fun setAlbums(albums: List<GooglePhotoAlbumModel>) {
        this.album.clear()
        this.album.addAll(albums)
        notifyDataSetChanged()
    }

}