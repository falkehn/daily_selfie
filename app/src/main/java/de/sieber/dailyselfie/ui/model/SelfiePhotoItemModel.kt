package de.sieber.dailyselfie.ui.model

import androidx.databinding.ObservableBoolean

class SelfiePhotoItemModel(
    var id: Long = 0,
    var googlePhotosId: String = "",
    var face: FaceModel? = null,
    val dateAsString: String = "",
    val filePath: String = "",
    val isEditing: ObservableBoolean = ObservableBoolean(false),
    val isSelected: ObservableBoolean = ObservableBoolean(false)
) : SelfieItemBaseModel()