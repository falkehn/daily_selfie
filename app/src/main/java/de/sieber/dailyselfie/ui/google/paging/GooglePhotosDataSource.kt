package de.sieber.dailyselfie.ui.google.paging

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.google.photos.library.v1.PhotosLibraryClient
import com.google.photos.library.v1.proto.ListMediaItemsRequest
import com.google.photos.types.proto.MediaItem
import de.sieber.dailyselfie.ui.extensions.getTimeString
import de.sieber.dailyselfie.ui.model.GooglePhotoModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext


const val PAGE_SIZE = 25

class GooglePhotosDataSource(
    private val googlePhotosLibraryClient: PhotosLibraryClient
) : PageKeyedDataSource<String, GooglePhotoModel>(), CoroutineScope {

    private val isInitialLoading = MutableLiveData<Boolean>()
    private val isPaging = MutableLiveData<Boolean>()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, GooglePhotoModel>
    ) {

        launch {
            isInitialLoading.postValue(true)

            val request = ListMediaItemsRequest.newBuilder()
                .setPageSize(PAGE_SIZE)
                .build()

            val response = googlePhotosLibraryClient.listMediaItems(request)
            val page = response.page
            val photos = page.response.mediaItemsList
                .filter { mediaItem -> mediaItem.mimeType.contains("image") }
                .map { mediaItem ->
                    createGooglePhoto(mediaItem)
                }

            callback.onResult(photos, null, response.nextPageToken)

            isInitialLoading.postValue(false)
        }

    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, GooglePhotoModel>) {
        launch {
            isPaging.postValue(true)
            val request = ListMediaItemsRequest.newBuilder()
                .setPageSize(PAGE_SIZE)
                .setPageToken(params.key)
                .build()
            val response = googlePhotosLibraryClient.listMediaItems(request)
            val page = response.page
            val photos = page.response.mediaItemsList
                .filter { mediaItem -> mediaItem.mimeType.contains("image") }
                .map { mediaItem ->
                    createGooglePhoto(mediaItem)
                }
            callback.onResult(photos, response.nextPageToken)

            isPaging.postValue(false)
        }
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, GooglePhotoModel>) = Unit

    fun isInitialLoading(): LiveData<Boolean> = isInitialLoading

    fun isPaging(): LiveData<Boolean> = isPaging

    private fun createGooglePhoto(mediaItem: MediaItem) = GooglePhotoModel(
        fileName = mediaItem.filename,
        smallImageUrl = mediaItem.baseUrl,
        largeImageUrl = "${mediaItem.baseUrl}=w2048-h1024",
        creationTimeAsString = mediaItem.mediaMetadata.creationTime.seconds.getTimeString(isInSeconds = true),
        creationTimeInSeconds = mediaItem.mediaMetadata.creationTime.seconds,
        id = mediaItem.id
    )

}