package de.sieber.dailyselfie.ui.selfies

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.provider.OpenableColumns
import androidx.core.content.FileProvider
import androidx.exifinterface.media.ExifInterface
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import de.sieber.dailyselfie.BuildConfig
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.FirebaseUtils
import de.sieber.dailyselfie.ui.common.GooglePhotosAlbum
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.extensions.getDisplayWidth
import de.sieber.dailyselfie.ui.extensions.getFilePath
import de.sieber.dailyselfie.ui.extensions.resizeByWidth
import de.sieber.dailyselfie.ui.extensions.rotate
import de.sieber.dailyselfie.ui.model.SelfieItemBaseModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale


class SelfiesViewModel(
    val application: Application,
    val albumRepository: AlbumRepository
) : ScopedViewModel(Dispatchers.IO) {

    private val isEditing = MutableLiveData<Boolean>().apply {
        this.postValue(false)
    }

    val albumId = MutableLiveData<Long>()

    val selfiesPaged: LiveData<PagedList<SelfieItemBaseModel>> = Transformations.switchMap(albumId) { albumId ->
        albumRepository.getPagingLiveData(albumId)
    }

    val selfies: LiveData<List<SelfieEntity>> = Transformations.switchMap(albumId) { albumId ->
        albumRepository.getSelfiesOfAlbumAsLiveData(albumId)
    }

    val isEmpty: LiveData<Boolean> = Transformations.map(selfies) { selfies ->
        selfies?.isEmpty() == true
    }

    val title: LiveData<String> = Transformations.map(albumId) { albumId ->
        if (albumId == 0L) {
            ""
        } else {
            val album = albumRepository.getAlbumWithSelfies(albumId)
            "${album?.albumEntity?.name}"
        }
    }

    fun toggleEditing(position: Int = -1) {
        val newState = isEditing.value?.not() ?: false
        selfiesPaged.value
            ?.filterIsInstance(SelfiePhotoItemModel::class.java)
            ?.forEachIndexed { index, selfieModel ->
                Timber.i("Index: $index, Selfie-Model: $selfieModel")
                selfieModel.let {
                    it.isEditing.set(newState)
                    it.isSelected.set(index == position - 1)
                }
            }

        this.isEditing.postValue(newState)
    }

    fun isEditing() = isEditing

    fun getSelfieForPosition(position: Int): SelfiePhotoItemModel? {
        return selfiesPaged.value?.get(position) as SelfiePhotoItemModel?
    }

    fun getSelectedSelfies() =
        selfiesPaged.value
            ?.filterIsInstance(SelfiePhotoItemModel::class.java)
            ?.filter { it.isSelected.get() }

    fun deleteSelectedSelfies() {
        getSelectedSelfies()
            ?.forEach { selfieItemModel ->
                selfieItemModel.let {
                    val file = File(it.filePath)
                    file.delete()
                    albumRepository.deleteSelfie(it.id)
                }
            }
        refreshSelfies()
    }

    fun addAlbumToFirebaseUser(googlePhotosAlbum: GooglePhotosAlbum) = launch {
        val firebaseUtils = FirebaseUtils()
        if (firebaseUtils.userLoggedIn()) {
            firebaseUtils.addAlbumToFirebaseUser(googlePhotosAlbum)
        }
    }

    fun deleteAlbumFromFirebaseUser(albumId: Long) = launch {
        val firebaseUtils = FirebaseUtils()
        if (firebaseUtils.userLoggedIn()) {
            val album = albumRepository.getAlbum(albumId)
            firebaseUtils.deleteAlbumFromFirebaseUser(album.googlePhotosAlbumId)
        }
    }

    fun deletePhotosOfAlbum(albumId: Long) {
        albumRepository.getAlbumWithSelfies(albumId)?.let { albumWithSelfies ->
            albumWithSelfies.selfies?.let { selfies ->
                selfies.forEach { selfie ->
                    val file = File(selfie.filePath)
                    file.delete()
                }
            }
        }
        albumRepository.deleteAlbumWithSelfies(albumId)
    }

    @Throws(IOException::class)
    fun getTempPhotoFile(context: Context): Uri {
        return FileProvider.getUriForFile(
            application,
            "${BuildConfig.APPLICATION_ID}.provider",
            File("${getFilePath(context)}/temp.jpg")
        )
    }

    fun getPhotoFilePath(context: Context): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val file = File("${getFilePath(context)}/$timeStamp.jpg")
        if (file.exists().not()) {
            file.createNewFile()
        }
        return file
    }

    fun addSelfieToAlbum(filePath: String): SelfieEntity {
        val albumId = getAlbumId()
        return albumRepository.addSelfie(albumId, filePath)
    }

    private fun getAlbumId(): Long {
        val preferencesHelper = PreferencesHelper(application)
        return preferencesHelper.lastSelectedSelfieAlbum
    }

    fun hasEnoughSelfiesToCreateVideo() = selfies.value?.size!! >= 5

    fun importPhotosFromDevice(uris: List<Uri>): List<SelfieEntity> {
        val localAlbumId = getAlbumId()
        val selfiesToUpload = mutableListOf<SelfieEntity>()
        val simpleDateFormat = SimpleDateFormat("yyyy:MM:dd hh:mm:ss", Locale.GERMANY)
        uris.forEach { uri ->
            val creationTime = getCreationTimeOfPhotosInSeconds(application, uri)
            val filePath = copyFileToPackage(uri, simpleDateFormat.format(Date(creationTime * 1000)))
            val selfie = SelfieEntity(
                albumId = localAlbumId,
                filePath = filePath,
                creationTimeInSeconds = creationTime
            )
            albumRepository.addSelfie(selfie)
            selfiesToUpload.add(selfie)
        }
        return selfiesToUpload
    }

    private fun copyFileToPackage(uri: Uri, date: String): String {
        // Try to open the file for "read" access using the
        // returned URI. If the file isn't found, write to the
        // error log and return.
        val inputPFD = try {
            // Get the content resolver instance for this context, and use it
            //to get a ParcelFileDescriptor for the file.
            application.contentResolver.openFileDescriptor(uri, "r")
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            null
        }

        // Get a regular file descriptor for the file
        val fd = inputPFD?.fileDescriptor
        val fileStream = FileInputStream(fd)

        val original = BitmapFactory.decodeFileDescriptor(fd)
        val width = application.getDisplayWidth()
        val scaledAndRotated = original.resizeByWidth(width.toInt()).rotate(fileStream)

        val fileName = getFileNameOfUri(application, uri)
        val filePath = "${getFilePath(application)}/$fileName"
        val dst = File(filePath)
        if (dst.exists().not()) {
            dst.createNewFile()
        }
        val outStream = FileOutputStream(dst)
        scaledAndRotated.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
        fileStream.close()
        outStream.close()

        val exif = ExifInterface(filePath)
        exif.setAttribute(ExifInterface.TAG_DATETIME, date)
        exif.saveAttributes()

        return dst.absolutePath
    }

    private fun getFileNameOfUri(context: Context, photoUri: Uri): String {
        val cursor = context.contentResolver.query(photoUri, null, null, null, null)

        return if (cursor != null && cursor.count >= 1) {
            cursor.moveToFirst()
            val title = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
            cursor.close()
            title
        } else {
            ""
        }
    }

    private fun getCreationTimeOfPhotosInSeconds(context: Context, photoUri: Uri): Long {
        val cursor = context.contentResolver.query(
            photoUri,
            arrayOf(
                MediaStore.Images.ImageColumns.DATE_TAKEN
            ), null, null, null
        )

        return if (cursor != null && cursor.count >= 1) {
            cursor.moveToFirst()
            val dateTaken = cursor.getLong(0)
            cursor.close()
            dateTaken / 1000
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis / 1000
        }
    }

    fun detectFace(selfieEntity: SelfieEntity) = launch {
        selfieEntity.detectFace(albumRepository)
    }

    fun refreshSelfies() = albumId.postValue(getAlbumId())

    fun getSelfiesForDetailedView(): List<SelfiePhotoItemModel> =
        selfiesPaged.value?.filterIsInstance(SelfiePhotoItemModel::class.java) ?: listOf()

}