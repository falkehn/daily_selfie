package de.sieber.dailyselfie.ui.extensions

import android.graphics.RectF
import com.google.android.gms.vision.face.Face

fun Face.getBoundingBox() = RectF()
    .apply {
        left = this@getBoundingBox.position.x
        top = this@getBoundingBox.position.y
        right = this@getBoundingBox.position.x + this@getBoundingBox.width
        bottom = this@getBoundingBox.position.y + this@getBoundingBox.height
    }