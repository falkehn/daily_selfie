package de.sieber.dailyselfie.ui.facetimelapse

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Rect
import android.media.RingtoneManager
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.BitmapToVideoEncoder
import de.sieber.dailyselfie.ui.common.FileUtils
import de.sieber.dailyselfie.ui.common.IntentUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.extensions.getTimeString
import de.sieber.dailyselfie.ui.extensions.getVideoFilePath
import de.sieber.dailyselfie.ui.model.FaceModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import timber.log.Timber
import java.io.File
import kotlin.coroutines.CoroutineContext


const val ACTION_IS_PROCESSING = "ACTION_IS_PROCESSING"
const val EXTRA_IS_PROCESSING = "EXTRA_IS_PROCESSING"
const val EXTRA_TIME_RANGE_FROM = "EXTRA_TIME_RANGE_FROM"
const val EXTRA_TIME_RANGE_TILL = "EXTRA_TIME_RANGE_TILL"
const val EXTRA_FRAMES_PER_SECOND = "EXTRA_FRAMES_PER_SECOND"
const val EXTRA_VIDEO_FACE_MAX_WIDTH = "EXTRA_VIDEO_FACE_MAX_WIDTH"
const val EXTRA_VIDEO_WIDTH = "EXTRA_VIDEO_WIDTH"
const val EXTRA_VIDEO_HEIGHT = "EXTRA_VIDEO_HEIGHT"
const val EXTRA_SHOW_DATES = "EXTRA_SHOW_DATES"
const val NOTIFICATION_VIDEO_IS_READY_ID = 4543534

class CreateVideoService : IntentService("CreateVideoService"), KoinComponent, CoroutineScope {

    private val albumRepository by inject<AlbumRepository>()

    private val dispatcher: CoroutineDispatcher = Dispatchers.IO

    override fun onHandleIntent(intent: Intent) {
        Timber.i("Start CreateVideoService")

        createVideo(
            timeRangeFrom = intent.getLongExtra(EXTRA_TIME_RANGE_FROM, 0),
            timeRangeTill = intent.getLongExtra(EXTRA_TIME_RANGE_TILL, 0),
            framesPerSecond = intent.getIntExtra(EXTRA_FRAMES_PER_SECOND, 0),
            videoFaceMaxWidth = intent.getFloatExtra(EXTRA_VIDEO_FACE_MAX_WIDTH, 0f),
            videoWidth = intent.getFloatExtra(EXTRA_VIDEO_WIDTH, 0f),
            videoHeight = intent.getFloatExtra(EXTRA_VIDEO_HEIGHT, 0f),
            showDates = intent.getBooleanExtra(EXTRA_SHOW_DATES, true)
        )

        Timber.i("Stop CreateVideoService")
    }

    private fun createVideo(
        timeRangeFrom: Long,
        timeRangeTill: Long,
        framesPerSecond: Int,
        videoFaceMaxWidth: Float,
        videoWidth: Float,
        videoHeight: Float,
        showDates: Boolean
    ) = launch {
        sendBroadcast(getIsProcessingIntent(isProcessing = true))

        try {
            val albumId = getAlbumId()
            // boundaries of face need to be all the same width/height
            val selfieEntities = albumRepository.getSelfieEntitiesForAlbumIdAsc(albumId)
                .filter {
                    it.creationTimeInSeconds in timeRangeFrom..timeRangeTill
                }

            // find faces incl. boundaries on image
            Timber.i("Start detecting Faces, Image: ${selfieEntities.size}")
            selfieEntities.forEach { selfie ->
                if (selfie.hasDetectedFace.not()) {
                    Timber.i("Detect Face for $selfie")
                    selfie.detectFace(albumRepository)
                }
            }

            val selfieItemModel = selfieEntities.map {
                SelfiePhotoItemModel(
                    id = it.id,
                    googlePhotosId = it.googlePhotosId,
                    face = getFaceModelOfSelfieEntity(it),
                    dateAsString = it.creationTimeInSeconds.getTimeString(isInSeconds = true),
                    filePath = it.filePath
                )
            }

            deleteVideoFile()
            val videoFile = getVideoFile()

            val bitmapToVideoEncoder = BitmapToVideoEncoder(BitmapToVideoEncoder.IBitmapToVideoEncoderCallback {
                sendBroadcast(getIsProcessingIntent(isProcessing = false))
                removeCreateVideoNotification()
                createVideoIsReadyNotification(albumId)
            })
            bitmapToVideoEncoder.startEncoding(videoFile, videoWidth.toInt(), videoHeight.toInt(), framesPerSecond)

            selfieItemModel
                .filter {
                    it.face != null
                }
                .forEachIndexed { index, selfie ->
                    try {
                        val bitmap = FileUtils().getBitmapFromFilePath(selfie.filePath)!!
                        val face = selfie.face!!
                        Timber.i("$index: ${selfie.dateAsString}")
                        val dateToShow = if (showDates) selfie.dateAsString else ""
                        val transformedBitmap = transform(bitmap, face, dateToShow, videoFaceMaxWidth, videoWidth, videoHeight)
                        bitmapToVideoEncoder.queueFrame(transformedBitmap)
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }
                }
            bitmapEndingWorkaround(bitmapToVideoEncoder, videoWidth, videoHeight)
            bitmapToVideoEncoder.stopEncoding()
        } catch (exception: Exception) {
            exception.printStackTrace()
            deleteVideoFile()
        }
    }

    private fun getVideoFile(): File {
        val albumId = getAlbumId()
        val videoFilePath = getVideoFilePath(application, albumId)
        return File(videoFilePath)
    }

    private fun deleteVideoFile() {
        val videoFile = getVideoFile()
        videoFile.delete()
    }

    /**
     * At the end of the Video, the last two Images are missing. With this function we can do a workaround.
     */
    private fun bitmapEndingWorkaround(
        bitmapToVideoEncoder: BitmapToVideoEncoder,
        videoWidth: Float,
        videoHeight: Float
    ) {
        bitmapToVideoEncoder.queueFrame(
            Bitmap.createBitmap(
                videoWidth.toInt(),
                videoHeight.toInt(),
                Bitmap.Config.ARGB_8888
            )
        )
        bitmapToVideoEncoder.queueFrame(
            Bitmap.createBitmap(
                videoWidth.toInt(),
                videoHeight.toInt(),
                Bitmap.Config.ARGB_8888
            )
        )
    }

    private fun removeCreateVideoNotification() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_CREATE_VIDEO_ID)
    }

    private fun createVideoIsReadyNotification(albumId: Long) {
        val title = getString(R.string.notification_video_creation_finished)
        val text = getString(R.string.notification_video_creation_finished_click_here)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val brandPrimaryColor = ContextCompat.getColor(this, R.color.primary)
        val tickerText = title + "\n" + text

        val resultPendingIntent = IntentUtils.getMainActivityPendingIntent(
            context = applicationContext,
            albumId = albumId,
            hashCode = tickerText.hashCode()
        )
        val channelId = "channel_01"
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setContentText(text)
            .setTicker(tickerText)
            .setStyle(NotificationCompat.BigTextStyle().bigText(text))
            .setSmallIcon(R.drawable.ic_stat_name)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setColor(brandPrimaryColor)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setLights(brandPrimaryColor, 5000, 5000)
            .setAutoCancel(true)
            .setContentIntent(resultPendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, title, NotificationManager.IMPORTANCE_DEFAULT)
            with(channel) {
                lightColor = brandPrimaryColor
                enableLights(true)
                enableVibration(true)
                setShowBadge(true)
                notificationManager.createNotificationChannel(this)
            }

            notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
        }

        val notification = notificationBuilder.build()
        notificationManager.notify(NOTIFICATION_VIDEO_IS_READY_ID, notification)
    }

    private fun getAlbumId(): Long {
        val preferencesHelper = PreferencesHelper(application)
        return preferencesHelper.lastSelectedSelfieAlbum
    }

    private fun transform(
        original: Bitmap,
        face: FaceModel,
        date: String,
        videoFaceMaxWidth: Float,
        videoWidth: Float,
        videoHeight: Float
    ): Bitmap {
        val scaleFactor = videoFaceMaxWidth / face.boundaries.width().toFloat()

        val videoCenterX = videoWidth / 2
        val videoCenterY = videoHeight / 2

        val faceCenterX = face.boundaries.centerX()
        val translateX = videoCenterX - faceCenterX

        val faceCenterY = face.boundaries.centerY()
        val translateY = videoCenterY - faceCenterY

        val rotate = face.headEulerAngleY

        val config = Bitmap.Config.ARGB_8888

        val matrix = Matrix()
        matrix.preScale(
            scaleFactor,
            scaleFactor,
            faceCenterX.toFloat(),
            faceCenterY.toFloat()
        )
        matrix.postRotate(rotate, faceCenterX.toFloat(), faceCenterY.toFloat())
        matrix.postTranslate(translateX, translateY)

        val result = Bitmap.createBitmap(videoWidth.toInt(), videoHeight.toInt(), config)
        result.eraseColor(Color.BLACK)

        val canvas = Canvas(result)
        canvas.drawBitmap(original, matrix, null)

        val textPaint = TextPaint()
        textPaint.isAntiAlias = true
        textPaint.textSize = 42 * resources.displayMetrics.density
        textPaint.color = Color.WHITE

        val alignment = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            Layout.Alignment.ALIGN_RIGHT
        } else {
            Layout.Alignment.ALIGN_NORMAL
        }

        val sb = StaticLayout.Builder.obtain(date, 0, date.length, textPaint, canvas.width)
            .setAlignment(alignment)
            .setIncludePad(false)
        val layout = sb.build()
        canvas.translate(0f, canvas.height - layout.height - 0.0f)
        layout.draw(canvas)

        return result
    }

    private fun getFaceModelOfSelfieEntity(selfieEntity: SelfieEntity): FaceModel? {
        return if (selfieEntity.hasDetectedFace) {
            FaceModel(
                selfieEntity.faceHeadEulerAngleY,
                Rect(selfieEntity.faceLeft, selfieEntity.faceTop, selfieEntity.faceRight, selfieEntity.faceBottom)
            )
        } else {
            null
        }
    }

    private fun getIsProcessingIntent(isProcessing: Boolean) = Intent().apply {
        action = ACTION_IS_PROCESSING
        putExtra(EXTRA_IS_PROCESSING, isProcessing)
    }

    //region Coroutine-Scope -------------------------------------------------------------------------------------------
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + dispatcher
    //endregion Coroutine-Scope

}