package de.sieber.dailyselfie.ui.facetimelapse

import android.app.DatePickerDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentCreateVideoBinding
import de.sieber.dailyselfie.ui.common.IntentUtils
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseFragment
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarModel
import de.sieber.dailyselfie.ui.main.MainActivity
import org.jetbrains.anko.alert
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.Calendar


const val NOTIFICATION_CREATE_VIDEO_ID = 23332

class CreateVideoFragment : SimpleBaseFragment() {

    private val createVideoPreferenceFragment by lazy {
        CreateVideoPreferenceFragment()
    }

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentCreateVideoBinding>(
                inflater,
                R.layout.fragment_create_video,
                container,
                false
            )
        ) {
            lifecycleOwner = this@CreateVideoFragment

            addPreferenceFragment(savedInstanceState)

            btnStartCreateVideo.onClick {
                showVideoCreationRunInBackgroundDialog()
                showVideoCreationRunInBackgroundNotification()
                findNavController().popBackStack(R.id.selfiesFragment, true)
                createVideoPreferenceFragment.startCreateVideoService()
            }

            setStatusBarColor(R.color.status_bar_color_red)

            return root
        }
    }
    //endregion Fragment-Lifecycle

    //region SimpleBaseFragment overrides ------------------------------------------------------------------------------------------------------------
    override fun createToolbar() =
        ToolbarModel.Builder()
            .withId(R.id.toolbarLayout)
            .withTitle(getString(R.string.generate_video))
            .withDefaultNavigationButton()
            .build()
    //endregion SimpleBaseFragment overrides

    private fun addPreferenceFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            val ft = childFragmentManager.beginTransaction()
            ft.add(R.id.prefContainer, createVideoPreferenceFragment)
            ft.commit()
        }
    }

    private fun showVideoCreationRunInBackgroundDialog() {
        AlertDialog.Builder(activity!!)
            .setMessage(getString(R.string.creation_will_take_a_while))
            .setPositiveButton(android.R.string.ok) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun showVideoCreationRunInBackgroundNotification() {
        context?.let {
            val title = getString(R.string.notification_video_creation)
            val notificationManager = it.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val brandPrimaryColor = ContextCompat.getColor(it, R.color.primary)
            val resultIntent = Intent(context, MainActivity::class.java)
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val resultPendingIntent =
                IntentUtils.getMainActivityPendingIntent(context = it, hashCode = title.hashCode())

            val channelId = "channel_01"
            val notificationBuilder = NotificationCompat.Builder(it, channelId)
                .setContentTitle(title)
                .setTicker(title)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setColor(brandPrimaryColor)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLights(brandPrimaryColor, 5000, 5000)
                .setOngoing(true)
                .setContentIntent(resultPendingIntent)

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelId, title, NotificationManager.IMPORTANCE_DEFAULT)
                with(channel) {
                    lightColor = brandPrimaryColor
                    enableLights(true)
                    enableVibration(true)
                    setShowBadge(true)
                    notificationManager.createNotificationChannel(this)
                }

                notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
            }

            val notification = notificationBuilder.build()
            notificationManager.notify(NOTIFICATION_CREATE_VIDEO_ID, notification)
        }
    }

    class CreateVideoPreferenceFragment : PreferenceFragmentCompat() {

        private val prefSpeed by lazy {
            findPreference<ListPreference>("PREF_SPEED")
        }
        private val prefUseAllPhotos by lazy {
            findPreference<SwitchPreference>("PREF_USE_ALL_PHOTOS")
        }
        private val prefPhotosTimeRangeFrom by lazy {
            findPreference<Preference>("PREF_USE_PHOTOS_FROM")
        }
        private val prefPhotosTimeRangeTill by lazy {
            findPreference<Preference>("PREF_USE_PHOTOS_TILL")
        }
        private val prefShowDates by lazy {
            findPreference<SwitchPreference>("PREF_SHOW_DATES")
        }

        private val vmCreateVideo by sharedViewModel<CreateVideoViewModel>()

        //region Fragment Lifecycle ------------------------------------------------------------------------------------------------------------------
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            addPreferencesFromResource(R.xml.create_video)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            setSpeed()
            setPhotosSelection()
            setDatesSettings()

            with(vmCreateVideo) {
                timeRangeFromAsString.observe(viewLifecycleOwner, Observer { timeRangeFrom ->
                    prefPhotosTimeRangeFrom?.title = timeRangeFrom
                })
                timeRangeTillAsString.observe(viewLifecycleOwner, Observer { timeRangeTill ->
                    prefPhotosTimeRangeTill?.title = timeRangeTill
                })
            }

        }
        //endregion Fragment Lifecycle

        private fun setSpeed() {
            prefSpeed?.let { speedPreference ->
                val speed = resources.getStringArray(R.array.speed)
                val normal = speed[1]
                speedPreference.setValueIndex(1)
                speedPreference.title = normal
                speedPreference.setOnPreferenceChangeListener { _, newValue ->
                    val newSpeed = when (speedPreference.findIndexOfValue(newValue.toString())) {
                        0 -> Speed.Slow
                        1 -> Speed.Normal
                        2 -> Speed.Quick
                        3 -> Speed.Fast
                        else -> Speed.Normal
                    }
                    vmCreateVideo.selectedSpeed = newSpeed
                    speedPreference.title = newValue.toString()
                    true
                }
            }
        }

        private fun setPhotosSelection() {
            prefUseAllPhotos?.let { useAllPhotos ->
                useAllPhotos.isChecked = true
                useAllPhotos.setOnPreferenceChangeListener { _, newValue ->
                    val timeRangeButtonVisible = (newValue as Boolean).not()
                    prefPhotosTimeRangeFrom?.isVisible = timeRangeButtonVisible
                    prefPhotosTimeRangeTill?.isVisible = timeRangeButtonVisible
                    vmCreateVideo.useAllPhotos.postValue(newValue)
                    true
                }
            }
            prefPhotosTimeRangeFrom?.isVisible = false
            prefPhotosTimeRangeFrom?.setOnPreferenceClickListener {
                showDatePickerFrom()
                true
            }
            prefPhotosTimeRangeTill?.isVisible = false
            prefPhotosTimeRangeTill?.setOnPreferenceClickListener {
                showDatePickerTill()
                true
            }
        }

        private fun setDatesSettings() {
            prefShowDates?.let { showDates ->
                showDates.isChecked = true
                showDates.setOnPreferenceChangeListener { _, newValue ->
                    vmCreateVideo.showDates = newValue as Boolean
                    true
                }
            }
        }

        private fun showDatePickerFrom() {
            val activity = activity ?: return

            val from = vmCreateVideo.selectedTimeRangeFrom.value
            from?.let {
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

                    val set = Calendar.getInstance()
                    set.set(Calendar.YEAR, year)
                    set.set(Calendar.MONTH, monthOfYear)
                    set.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    val till = vmCreateVideo.selectedTimeRangeTill.value
                    till?.let {
                        if (set.after(till)) {
                            activity.alert {
                                message = "Please check your Dates."
                                okButton {
                                    it.dismiss()
                                }
                            }.show()
                        } else {
                            vmCreateVideo.selectedTimeRangeFrom.postValue(set)
                        }
                    }

                }, from.get(Calendar.YEAR), from.get(Calendar.MONTH), from.get(Calendar.DAY_OF_MONTH)).apply {
                    datePicker.minDate = vmCreateVideo.availableTimeRangeFrom.value?.timeInMillis ?: 0
                    datePicker.maxDate = vmCreateVideo.availableTimeRangeTill.value?.timeInMillis ?: 0
                    show()
                }
            }
        }

        private fun showDatePickerTill() {
            val activity = activity ?: return

            val till = vmCreateVideo.selectedTimeRangeTill.value
            till?.let {
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

                    val set = Calendar.getInstance()
                    set.set(Calendar.YEAR, year)
                    set.set(Calendar.MONTH, monthOfYear)
                    set.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    val from = vmCreateVideo.selectedTimeRangeFrom.value
                    from?.let {
                        if (set.before(from)) {
                            activity.alert {
                                message = "Please check your Dates."
                                okButton {
                                    it.dismiss()
                                }
                            }.show()
                        } else {
                            vmCreateVideo.selectedTimeRangeTill.postValue(set)
                        }
                    }

                }, till.get(Calendar.YEAR), till.get(Calendar.MONTH), till.get(Calendar.DAY_OF_MONTH)).apply {
                    datePicker.minDate = vmCreateVideo.availableTimeRangeFrom.value?.timeInMillis ?: 0
                    datePicker.maxDate = vmCreateVideo.availableTimeRangeTill.value?.timeInMillis ?: 0
                    show()
                }
            }
        }

        fun startCreateVideoService() {
            activity?.let {
                val intent = Intent(it, CreateVideoService::class.java).apply {
                    putExtra(EXTRA_TIME_RANGE_FROM, vmCreateVideo.getTimeRangeFromInSeconds())
                    putExtra(EXTRA_TIME_RANGE_TILL, vmCreateVideo.getTimeRangeTillInSeconds())
                    putExtra(EXTRA_VIDEO_WIDTH, vmCreateVideo.getVideoWidth())
                    putExtra(EXTRA_VIDEO_HEIGHT, vmCreateVideo.getVideoHeight())
                    putExtra(EXTRA_VIDEO_FACE_MAX_WIDTH, vmCreateVideo.getVideoFaceMaxWidth())
                    putExtra(EXTRA_FRAMES_PER_SECOND, vmCreateVideo.getFramesPerSecond())
                    putExtra(EXTRA_SHOW_DATES, vmCreateVideo.showDates)
                }
                it.startService(intent)
            }
        }
    }
}