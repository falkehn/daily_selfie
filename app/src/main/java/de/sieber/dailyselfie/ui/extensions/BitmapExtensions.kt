package de.sieber.dailyselfie.ui.extensions

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import androidx.exifinterface.media.ExifInterface
import java.io.InputStream

fun Bitmap.resizeByWidth(maxWidth: Int): Bitmap {
    val outWidth: Int
    val outHeight: Int
    val inWidth = width
    val inHeight = height
    if (inWidth > inHeight) {
        outWidth = maxWidth
        outHeight = inHeight * maxWidth / inWidth
    } else {
        outHeight = maxWidth
        outWidth = inWidth * maxWidth / inHeight
    }

    return Bitmap.createScaledBitmap(this, outWidth, outHeight, false)
}

fun Bitmap.rotate(inputStream: InputStream): Bitmap {
    val exif = ExifInterface(inputStream)
    val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
    val matrix = Matrix()
    when (orientation) {
        6 -> matrix.postRotate(90f)
        3 -> matrix.postRotate(180f)
        8 -> matrix.postRotate(270f)
    }
    return Bitmap.createBitmap(this, 0, 0, this.width, this.height, matrix, true)
}

fun decodeSampledBitmapFromResource(
    res: Resources,
    resId: Int,
    reqWidth: Int,
    reqHeight: Int
): Bitmap {
    // First decode with inJustDecodeBounds=true to check dimensions
    return BitmapFactory.Options().run {
        inJustDecodeBounds = true
        BitmapFactory.decodeResource(res, resId, this)

        // Calculate inSampleSize
        inSampleSize = calculateInSampleSize(this, reqWidth, reqHeight)

        outWidth = reqWidth
        val ratio: Float = reqWidth.toFloat() / reqWidth.toFloat()
        outHeight = Math.round(reqWidth / ratio)

        // Decode bitmap with inSampleSize set
        inJustDecodeBounds = false

        BitmapFactory.decodeResource(res, resId, this)
    }
}

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val (height: Int, width: Int) = options.run { outHeight to outWidth }
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {

        val halfHeight: Int = height / 2
        val halfWidth: Int = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}