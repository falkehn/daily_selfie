package de.sieber.dailyselfie.ui.model

data class GalleryModel(
    val id: Long = 0,
    val name: String = "",
    val timeRange: String = "",
    val countOfSelfies: Int = 0
)