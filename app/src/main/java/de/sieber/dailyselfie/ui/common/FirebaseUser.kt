package de.sieber.dailyselfie.ui.common

data class FirebaseUser(
    val uuid: String = "",
    var notificationSettings: NotificationSettings = NotificationSettings(),
    var googlePhotosAlbum: List<GooglePhotosAlbum> = mutableListOf(),
    var premiumAccount: Boolean = true
)

data class GooglePhotosAlbum(
    val name: String = "",
    val id: String = ""
)

data class NotificationSettings(
    val enabled: Boolean = true,
    val time: String = ""
)