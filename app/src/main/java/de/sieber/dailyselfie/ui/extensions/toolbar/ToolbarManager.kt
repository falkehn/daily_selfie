package de.sieber.dailyselfie.ui.extensions.toolbar

import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.google.android.material.appbar.AppBarLayout
import de.sieber.dailyselfie.databinding.ViewToolbarBinding
import kotlinx.android.synthetic.main.view_toolbar.view.*

/**
 * A Utility Class that builds the Toolbar using the [ToolbarModel].
 *
 * @param lifecycleOwner the [FragmentActivity] or the [Fragment] where the Toolbar will be displayed.
 * @param container the View-Container in which the [com.lidl.eci.R.layout.view_toolbar] layout is located.
 * @param toolbarModel the model how the Toolbar should be build
 */
class ToolbarManager constructor(
    private var lifecycleOwner: LifecycleOwner,
    private var container: View,
    private var toolbarModel: ToolbarModel
) {

    /**
     * Call this Function in the [FragmentActivity.onCreate] or [Fragment.onViewCreated] to start the Toolbar.
     */
    fun prepareToolbar() {
        if (toolbarModel.resId != ToolbarModel.NO_TOOLBAR) {
            // find the Toolbar-View
            val toolbarLayout = container.findViewById<View>(toolbarModel.resId)
            // setup the databinding
            var toolbarDatabinding = DataBindingUtil.getBinding<ViewToolbarBinding>(toolbarLayout)
            if (toolbarDatabinding == null) {
                toolbarDatabinding = ViewToolbarBinding.bind(toolbarLayout)
            }

            toolbarDatabinding?.let {
                if (it.toolbar == null) {
                    it.toolbar = toolbarModel
                }
                it.setLifecycleOwner(lifecycleOwner)

                with(toolbarModel) {
                    val toolbar = toolbarLayout.toolbarWidget
                    prepareMenu(toolbar, this)
                    prepareNavigation(toolbar, this)
                }

                prepareScrollBehavior(it)

                it.executePendingBindings()
            }
        }
    }

    private fun prepareMenu(toolbar: Toolbar, toolbarModel: ToolbarModel) {
        with(toolbarModel) {
            // menu
            menuResId
                .observe(lifecycleOwner, Observer {
                    toolbar.menu.clear()
                    if (it != ToolbarModel.PARAMETER_NOT_SET) {
                        toolbar.inflateMenu(it)
                    }
                })

            menuItemClickListener?.let {
                toolbar.setOnMenuItemClickListener { menuItem: MenuItem? ->
                    menuItem?.let { item ->
                        menuItemClickListener.invoke(item)
                    }
                    false
                }
            }
        }
    }

    private fun prepareNavigation(toolbar: Toolbar, toolbarModel: ToolbarModel) {
        with(toolbarModel) {
            // navigation
            if (hasNavigationButton) {
                toolbar.setNavigationOnClickListener { backButtonView ->
                    // no special back navigation needed, use default behavior
                    if (backButtonClickListener == null) {
                        if (lifecycleOwner is FragmentActivity) {
                            (lifecycleOwner as FragmentActivity).onBackPressed()
                        } else if (lifecycleOwner is Fragment) {
                            (lifecycleOwner as Fragment).activity?.onBackPressed()
                        }
                    } else {
                        backButtonClickListener.invoke(backButtonView)
                    }
                }
            }
        }
    }

    private fun prepareScrollBehavior(toolbarDatabinding: ViewToolbarBinding) {
        // scroll-behavior
        val layoutParams = toolbarDatabinding.llToolbarContainer.layoutParams as AppBarLayout.LayoutParams
        when (toolbarModel.scrollBehavior) {
            ToolbarScrollBehavior.FIXED -> layoutParams.scrollFlags = 0
            ToolbarScrollBehavior.SCROLL_OUT ->
                layoutParams.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or
                        AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
        }
    }
}
