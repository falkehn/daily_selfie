package de.sieber.dailyselfie.ui.common

import android.content.Context

class PreferencesHelper(context: Context) {

    companion object {
        private const val NOTIFICATION = "NOTIFICATION"
        private const val NOTIFICATION_TIME = "NOTIFICATION_TIME"
        private const val ON_BOARDING_FINISHED = "ON_BOARDING_FINISHED"
        private const val LAST_SELECTED_GALLERY_ID = "LAST_SELECTED_GALLERY_ID"
    }

    private val preferences = context.getSharedPreferences("shared_prefs_selfies", Context.MODE_PRIVATE)

    var notificationsEnabled: Boolean
        get() = preferences.getBoolean(NOTIFICATION, true)
        set(value) = preferences.edit().putBoolean(NOTIFICATION, value).apply()

    var notificationsTime: String
        get() = preferences.getString(NOTIFICATION_TIME, "08:00") ?: "08:00"
        set(value) = preferences.edit().putString(NOTIFICATION_TIME, value).apply()

    var onBoardingFinished: Boolean
        get() = preferences.getBoolean(ON_BOARDING_FINISHED, false)
        set(value) = preferences.edit().putBoolean(ON_BOARDING_FINISHED, value).apply()


    var lastSelectedSelfieAlbum: Long
        get() = preferences.getLong(LAST_SELECTED_GALLERY_ID, 0)
        set(value) = preferences.edit().putLong(LAST_SELECTED_GALLERY_ID, value).apply()

}