package de.sieber.dailyselfie.ui.extensions

import android.content.Context
import android.net.Uri
import androidx.exifinterface.media.ExifInterface
import java.io.*


fun Uri.getImagePathFromInputStreamUri(context: Context): String? {
    var inputStream: InputStream? = null
    var filePath: String? = null

    if (authority != null) {
        try {
            inputStream = context.contentResolver.openInputStream(this) // context needed
            val photoFile = createTemporalFileFrom(context, inputStream)
//            copyExif(this.path!!, photoFile!!.absolutePath)
            filePath = photoFile!!.path
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                inputStream!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    return filePath
}

@Throws(IOException::class)
private fun createTemporalFileFrom(context: Context, inputStream: InputStream?): File? {
    var targetFile: File? = null

    if (inputStream != null) {
        val buffer = ByteArray(8 * 1024)

        targetFile = createTemporalFile(context)
        val outputStream = FileOutputStream(targetFile)

        var bytesRead = 0
        while (inputStream.read(buffer).also { bytesRead = it } >= 0) {
            outputStream.write(buffer, 0, bytesRead)
        }
        val exif = ExifInterface(inputStream)

        outputStream.flush()

        try {
            outputStream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    return targetFile
}

private fun createTemporalFile(context: Context): File {
    return File(context.externalCacheDir, "tempFile.jpg")
}

//@Throws(IOException::class)
//fun copyExif(originalPath: String, newPath: String) {
//    val attributes = arrayOf(
//        ExifInterface.TAG_DATETIME,
//        ExifInterface.TAG_DATETIME_DIGITIZED,
//        ExifInterface.TAG_EXPOSURE_TIME,
//        ExifInterface.TAG_FLASH,
//        ExifInterface.TAG_FOCAL_LENGTH,
//        ExifInterface.TAG_GPS_ALTITUDE,
//        ExifInterface.TAG_GPS_ALTITUDE_REF,
//        ExifInterface.TAG_GPS_DATESTAMP,
//        ExifInterface.TAG_GPS_LATITUDE,
//        ExifInterface.TAG_GPS_LATITUDE_REF,
//        ExifInterface.TAG_GPS_LONGITUDE,
//        ExifInterface.TAG_GPS_LONGITUDE_REF,
//        ExifInterface.TAG_GPS_PROCESSING_METHOD,
//        ExifInterface.TAG_GPS_TIMESTAMP,
//        ExifInterface.TAG_MAKE,
//        ExifInterface.TAG_MODEL,
//        ExifInterface.TAG_ORIENTATION,
//        ExifInterface.TAG_SUBSEC_TIME,
//        ExifInterface.TAG_WHITE_BALANCE
//    )
//
//    val oldExif = ExifInterface(originalPath)
//    val newExif = ExifInterface(newPath)
//
//    if (attributes.isNotEmpty()) {
//        for (i in attributes.indices) {
//            val value = oldExif.getAttribute(attributes[i])
//            if (value != null)
//                newExif.setAttribute(attributes[i], value)
//        }
//        newExif.saveAttributes()
//    }
//}