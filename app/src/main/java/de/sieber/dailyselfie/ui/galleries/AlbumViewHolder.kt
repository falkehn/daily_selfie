package de.sieber.dailyselfie.ui.galleries

import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.databinding.ItemAlbumBinding
import de.sieber.dailyselfie.ui.model.GalleryModel
import org.jetbrains.anko.sdk25.coroutines.onClick

class AlbumViewHolder(
    private var binding: ItemAlbumBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(
        galleryModel: GalleryModel,
        onGalleryClick: (galleryModel: GalleryModel) -> Unit
    ) {
        with(binding) {
            gallery = galleryModel
            executePendingBindings()
            rlGalleryItemContainer.onClick {
                onGalleryClick(galleryModel)
            }
        }
    }
}