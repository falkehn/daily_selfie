package de.sieber.dailyselfie.ui.google.paging

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import de.sieber.dailyselfie.ui.model.GooglePhotoModel

data class GooglePhotosListing(
    // the LiveData of paged lists for the UI to observe
    val pagedList: LiveData<PagedList<GooglePhotoModel>>,
    // represents the initial request status to show
    val isInitialLoading: LiveData<Boolean>,
    // represents the loading state when requesting the next page
    val isPaging: LiveData<Boolean>
)