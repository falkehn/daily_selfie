package de.sieber.dailyselfie.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.selfies.RC_GOOGLE_SIGN_IN
import kotlinx.android.synthetic.main.activity_main.navHostFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

const val EXTRA_OPEN_VIDEO = "EXTRA_OPEN_VIDEO"

class MainActivity : AppCompatActivity() {

    private val vmGooglePhotos by viewModel<GooglePhotosViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try {
            vmGooglePhotos.createGoogleApiClient(this)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

        if (vmGooglePhotos.alreadyLoggedIn()) {
            if (vmGooglePhotos.hasValidAuthToken()) {
                vmGooglePhotos.initializePhotoLibraryClient()
            } else {
                updateAuthToken()
            }
        }

        // if only one gallery exist, automatically push to the selfie-fragment
        // otherwise show list of albums
        val navHostFragment = navHostFragment as NavHostFragment
        val inflater = navHostFragment.navController.navInflater
        val graph = inflater.inflate(R.navigation.navigation)
        graph.startDestination = R.id.selfiesFragment
        navHostFragment.navController.graph = graph

        if (intent.getLongExtra(EXTRA_OPEN_VIDEO, 0) != 0L) {
            navHostFragment.navController.navigate(R.id.action_selfiesFragment_to_videoFragment)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            data?.let {
                vmGooglePhotos.handleSignInResult(
                    data = it,
                    onSuccess = {
                        vmGooglePhotos.initializePhotoLibraryClient()
                        vmGooglePhotos.syncPhotos()
                    },
                    onFailure = {
                        Timber.e("An error occured.")
                    }
                )

            }
        }

        val navHost = supportFragmentManager.findFragmentById(R.id.navHostFragment)
        navHost?.let { navFragment ->
            navFragment.childFragmentManager.primaryNavigationFragment?.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun updateAuthToken() {
        val intent = vmGooglePhotos.getSignInIntent()
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN)
    }
}
