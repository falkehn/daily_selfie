//package de.sieber.dailyselfie.ui.menu
//
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.databinding.DataBindingUtil
//import androidx.fragment.app.Fragment
//import androidx.lifecycle.Observer
//import androidx.navigation.fragment.findNavController
//import de.sieber.dailyselfie.R
//import de.sieber.dailyselfie.databinding.FragmentMenuBinding
//import de.sieber.dailyselfie.ui.facetimelapse.CreateVideoViewModel
//import de.sieber.dailyselfie.ui.google.fragment.GooglePhotosAllPhotosFragment
//import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
//import de.sieber.dailyselfie.ui.login.GoogleLoginFragment
//import kotlinx.android.synthetic.main.fragment_menu.btOpenCamera
//import kotlinx.android.synthetic.main.fragment_menu.gallery
//import org.jetbrains.anko.sdk25.coroutines.onClick
//import org.koin.androidx.viewmodel.ext.android.sharedViewModel
//import org.koin.androidx.viewmodel.ext.android.viewModel
//
//const val RC_GOOGLE_SIGN_IN = 999
//
//class MenuFragment : Fragment() {
//
//    private val vmFaceTimeLapse: CreateVideoViewModel by viewModel()
//
//    private val vmGoogleLogin by sharedViewModel<GooglePhotosViewModel>()
//
//    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        with(
//            DataBindingUtil.inflate<FragmentMenuBinding>(
//                inflater,
//                R.layout.fragment_menu,
//                container,
//                false
//            )
//        ) {
//            //            viewModel = vmProductOverview
////            setLifecycleOwner(this@ProductOverviewFragment)
////            lifecycle.addObserver(vmProductOverview)
//
//            return root
//        }
//    }
//
//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        btOpenCamera.onClick {
//            findNavController().navigate(R.id.action_menuFragment_to_cameraFragment2)
//        }
//        gallery.onClick {
//            if (vmGoogleLogin.isUserSignedIn()) {
//                showGooglePhotosGallery()
//            } else {
//                showGoogleLoginScreen()
//            }
//        }
//        vmGoogleLogin.userSignedIn.observe(viewLifecycleOwner, Observer { isUserSignedIn ->
//            isUserSignedIn.getContentIfNotHandled()?.let { userSignedIn ->
//                if (userSignedIn) {
//                    showGooglePhotosGallery()
//                }
//            }
//        })
////        context?.let {
////            vmFaceTimeLapse.selfieSample.observe(this@MenuFragment, Observer { selfie ->
////                imageView.setImageBitmap(selfie.bitmap)
////            })
////            vmFaceTimeLapse.createVideo(it)
////        }
//
//    }
//
//    private fun showGoogleLoginScreen() {
//        activity?.let {
//            val fragmentTransaction = it.supportFragmentManager.beginTransaction()
//            val googleLoginFragment = GoogleLoginFragment()
//            googleLoginFragment.show(fragmentTransaction, "GoogleLoginFragment")
//        }
//    }
//
//    private fun showGooglePhotosGallery() {
//        activity?.let {
//            val fragmentTransaction = it.supportFragmentManager.beginTransaction()
//            val googlePhotosGalleryFragment = GooglePhotosAllPhotosFragment()
//            googlePhotosGalleryFragment.show(fragmentTransaction, "GooglePhotosAllPhotosFragment")
//        }
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == RC_GOOGLE_SIGN_IN) {
//            if (resultCode == Activity.RESULT_OK) {
//                findNavController().navigate(R.id.action_menuFragment_to_googlePhotosGalleryFragment)
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                // TODO
//            }
//        }
//    }
//
//}