package de.sieber.dailyselfie.ui.galleries

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.model.GalleryModel

class AlbumAdapter(
    private val onGalleryClick: (galleryModel: GalleryModel) -> Unit
) : RecyclerView.Adapter<AlbumViewHolder>() {

    private val galleries = mutableListOf<GalleryModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        return AlbumViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_album, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return galleries.size
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(galleries[position], onGalleryClick)
    }

    fun setGalleries(galleries: List<GalleryModel>) {
        this.galleries.clear()
        this.galleries.addAll(galleries)
        notifyDataSetChanged()
    }

}