package de.sieber.dailyselfie.ui.onboarding

import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseFragment

abstract class BaseOnBoardingFragment : SimpleBaseFragment() {

    abstract fun playAnimation()

}