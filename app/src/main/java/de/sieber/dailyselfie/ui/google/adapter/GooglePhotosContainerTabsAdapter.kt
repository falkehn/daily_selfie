package de.sieber.dailyselfie.ui.google.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class GooglePhotosContainerTabsAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    // the first parameter of the pair is the title of the tab
    private val fragments: MutableList<Pair<String, Fragment>> = mutableListOf()

    override fun getItem(position: Int): Fragment = fragments[position].second

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence = fragments[position].first

    fun addFragment(title: String, fragment: Fragment) = fragments.add(Pair(title, fragment))

}