package de.sieber.dailyselfie.ui.preferences.viewmodel

import com.firebase.ui.auth.AuthUI
import de.sieber.dailyselfie.ui.common.FirebaseUtils
import de.sieber.dailyselfie.ui.common.NotificationUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import kotlinx.coroutines.Dispatchers
import java.util.Arrays

class PrefsViewModel(
    private val preferencesHelper: PreferencesHelper,
    private val notifcationUtils: NotificationUtils
) : ScopedViewModel(Dispatchers.IO) {

    private val firebaseUtils = FirebaseUtils()

    //region Login -----------------------------------------------------------------------------------------------------------------------------------
    fun isLoggedIn() = firebaseUtils.userLoggedIn()

    fun logout() = firebaseUtils.logout()

    fun getFirebaseAuthenticationProviders(): MutableList<AuthUI.IdpConfig> = Arrays.asList(
        AuthUI.IdpConfig.GoogleBuilder().build()
    )
    //endregion Login

    //region Notification ----------------------------------------------------------------------------------------------------------------------------
    fun areNotificationsEnabled() = preferencesHelper.notificationsEnabled

    fun setNotificationsEnabled(enabled: Boolean) {
        preferencesHelper.notificationsEnabled = enabled
        if (enabled) {
            val notificationTime = getNotificationTime()
            notifcationUtils.createRepeatingAlarmManager(notificationTime)
        } else {
            notifcationUtils.cancelAlarmManager()
        }
    }

    fun setNotificationTime(hourAndMinute: String) {
        preferencesHelper.notificationsTime = hourAndMinute
        notifcationUtils.createRepeatingAlarmManager(hourAndMinute)
    }

    fun getNotificationTime() = preferencesHelper.notificationsTime
    //endregion Notification
}