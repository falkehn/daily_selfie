package de.sieber.dailyselfie.ui.preferences.view

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.firebase.ui.auth.AuthUI
import com.marcoscg.licenser.Library
import com.marcoscg.licenser.License
import com.marcoscg.licenser.LicenserDialog
import de.sieber.dailyselfie.BuildConfig
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarManager
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarModel
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.onboarding.RC_SIGN_IN
import de.sieber.dailyselfie.ui.preferences.viewmodel.PrefsViewModel
import de.sieber.dailyselfie.ui.selfies.RC_GOOGLE_SIGN_IN
import kotlinx.android.synthetic.main.activity_prefs.llPrefsContainer
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


class PrefsActivity : AppCompatActivity() {

    private val preferenceFragment by lazy { PrefsFragment() }

    //region Activity Lifecyle ----------------------------------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prefs)

        addToolbar()
        addPreferenceFragment(savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        preferenceFragment.onActivityResult(requestCode, resultCode, data)
    }
    //endregion Activity Lifecyle

    private fun addToolbar() {
        val toolbarModel = ToolbarModel.Builder()
            .withId(R.id.toolbarLayout)
            .withTitle(getString(R.string.menu_preferences))
            .withDefaultNavigationButton()
            .build()
        ToolbarManager(this, llPrefsContainer, toolbarModel).prepareToolbar()
    }

    private fun addPreferenceFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.add(R.id.pref_container, preferenceFragment)
            ft.commit()
        }
    }

    class PrefsFragment : PreferenceFragmentCompat() {

        private val vmPrefs by viewModel<PrefsViewModel>()
        private val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()

        //region Fragment Lifecycle ------------------------------------------------------------------------------------
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            addPreferencesFromResource(R.xml.preferences)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            setLoginState()
            setNotificationState()
            setNotificationTimeState()
            setLicences()
            setFeedback()
            setAppRating()
            addVersion()
            setStatusBarColor(R.color.status_bar_color_red)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == RC_SIGN_IN) {
                if (resultCode == Activity.RESULT_OK) {
                    // Successfully signed in
                    signInForGooglePhotos()
                } else {
                    // Sign in failed. If response is null the user canceled the
                    // sign-in flow using the back button. Otherwise check
                    // response.getError().getErrorCode() and handle the error.
                    // ...
                }
            } else if (requestCode == RC_GOOGLE_SIGN_IN) {
                // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
                // The Task returned from this call is always completed, no need to attach
                // a listener.
                data?.let {
                    vmGooglePhotos.handleSignInResult(
                        data = it,
                        onSuccess = {
                            vmGooglePhotos.userSignedIn.postValue(Event(true))
                            activity?.runOnUiThread {
                                setLoginState()
                            }
                        },
                        onFailure = {
                            vmGooglePhotos.userSignedIn.postValue(Event(false))
                            activity?.runOnUiThread {
                                setLoginState()
                            }
                        }
                    )
                }
            }
        }
        //endregion Fragment Lifecycle

        private fun addVersion() {
            findPreference<Preference>("PREF_VERSION")?.let { preference ->
                preference.title = getString(R.string.pref_version, BuildConfig.VERSION_NAME)
            }
        }

        private fun setLoginState() {
            findPreference<Preference>("PREF_LOGIN_LOGOUT")?.let { preference ->
                val isLoggedIn = vmPrefs.isLoggedIn()
                val title = if (isLoggedIn) getString(R.string.pref_logout) else getString(R.string.pref_login)
                preference.title = title
                preference.setOnPreferenceClickListener {
                    if (isLoggedIn) {
                        logoutUser()
                        preference.title = getString(R.string.pref_login)
                    } else {
                        loginUser()
                    }
                    true
                }
            }
        }

        private fun loginUser() {
            // Create and launch sign-in intent
            val providers = vmPrefs.getFirebaseAuthenticationProviders()
            startActivityForResult(
                AuthUI.getInstance()
                    .createSignInIntentBuilder()
                    .setAvailableProviders(providers)
                    .setLogo(R.drawable.ic_launcher_background)
                    .build(),
                RC_SIGN_IN
            )
        }

        private fun signInForGooglePhotos() {
            activity?.let {
                vmGooglePhotos.createGoogleApiClient(it)
            }
            val intent = vmGooglePhotos.getSignInIntent()
            startActivityForResult(intent, RC_GOOGLE_SIGN_IN)
        }

        private fun logoutUser() {
            vmPrefs.logout()
            vmGooglePhotos.logout()
        }

        private fun setNotificationState() {
            findPreference<SwitchPreference>("PREF_REMINDER_ENABLE")?.let { preference ->
                preference.isChecked = vmPrefs.areNotificationsEnabled()
                preference.setOnPreferenceChangeListener { _, notificationsEnabled ->
                    vmPrefs.setNotificationsEnabled(notificationsEnabled as Boolean)
                    setNotificationTimeState()
                    true
                }
            }
        }

        private fun setNotificationTimeState() {
            findPreference<Preference>("PREF_REMINDER_TIME")?.let { preference ->
                preference.isVisible = vmPrefs.areNotificationsEnabled()
                preference.setOnPreferenceClickListener {
                    showNotificationTimePicker()
                    true
                }
                preference.summary = vmPrefs.getNotificationTime()
            }
        }

        private fun showNotificationTimePicker() {
            val calendar = Calendar.getInstance()
            TimePickerDialog(
                context,
                R.style.date_and_time_picker,
                TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                    val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
                    calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    calendar.set(Calendar.MINUTE, minute)
                    val time = simpleDateFormat.format(calendar.time)
                    vmPrefs.setNotificationTime(time)
                    setNotificationTimeState()
                },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            ).show()
        }

        private fun setFeedback() {
            findPreference<Preference>("PREF_FEEDBACK")?.let { preference ->
                preference.setOnPreferenceClickListener {
                    val feedbackIntent = Intent(Intent.ACTION_SEND).apply {
                        putExtra(Intent.EXTRA_EMAIL, arrayOf("dev.sieber@gmail.com"))
                        val versionName = getString(R.string.pref_version, BuildConfig.VERSION_NAME)
                        putExtra(Intent.EXTRA_SUBJECT, "Feedback ($versionName) - ${getString(R.string.app_name)}")
                        type = "plain/text"
                    }
                    activity?.startActivity(feedbackIntent)
                    true
                }
            }
        }

        private fun setAppRating() {
            findPreference<Preference>("PREF_RATE")?.let { preference ->
                preference.setOnPreferenceClickListener {
                    val uri = Uri.parse("market://details?id=de.sieber.dailyselfie")
                    activity?.startActivity(Intent(Intent.ACTION_VIEW, uri))
                    true
                }
            }
        }

        private fun setLicences() {
            findPreference<Preference>("PREF_LICENCES")?.let { preference ->
                preference.setOnPreferenceClickListener {
                    LicenserDialog(context)
                        .setTitle(getString(R.string.pref_licences))
                        .setLibrary(
                            Library(
                                "Android Support Libraries",
                                "https://developer.android.com/topic/libraries/support-library/index.html",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Anko",
                                "https://github.com/Kotlin/anko",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "RetroFit",
                                "https://square.github.io/retrofit/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "OkHttp",
                                "https://square.github.io/okhttp/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Glide",
                                "https://github.com/bumptech/glide/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Koin",
                                "https://github.com/InsertKoinIO/koin/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Lottie Animation",
                                "https://github.com/airbnb/lottie-android/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Rate this App",
                                "https://github.com/kobakei/Android-RateThisApp/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "kotlin-permissions",
                                "https://github.com/superjobru/kotlin-permissions/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "UCrop",
                                "https://github.com/krokyze/uCrop-n-Edit/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Fetch2",
                                "https://github.com/tonyofrancis/Fetch/",
                                License.APACHE
                            )
                        )
                        .setLibrary(
                            Library(
                                "Licenser",
                                "https://github.com/marcoscgdev/Licenser/",
                                License.MIT
                            )
                        )

                        .show()
                    true
                }
            }
        }

        private fun setStatusBarColor(@ColorRes color: Int) {
            activity?.window?.let { window ->
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(context!!, color)
            }
        }
    }

}