package de.sieber.dailyselfie.ui.selfies

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.stfalcon.imageviewer.StfalconImageViewer
import com.yalantis.ucrop.UCrop
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentSelfiesBinding
import de.sieber.dailyselfie.ui.common.GooglePhotosAlbum
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseFragment
import de.sieber.dailyselfie.ui.extensions.coroutineScope
import de.sieber.dailyselfie.ui.extensions.getDisplayHeight
import de.sieber.dailyselfie.ui.extensions.getDisplayWidth
import de.sieber.dailyselfie.ui.extensions.hasVideoForAlbum
import de.sieber.dailyselfie.ui.extensions.newInstanceOf
import de.sieber.dailyselfie.ui.galleries.AlbumViewModel
import de.sieber.dailyselfie.ui.galleries.BottomNavigationDrawerFragment
import de.sieber.dailyselfie.ui.google.fragment.GooglePhotosContainerFragment
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel
import de.sieber.dailyselfie.ui.preferences.view.PrefsActivity
import kotlinx.android.synthetic.main.fragment_selfies.bottomAppBar
import kotlinx.android.synthetic.main.fragment_selfies.fab
import kotlinx.android.synthetic.main.fragment_selfies.rvSelfies
import kotlinx.android.synthetic.main.view_empty_albums.btnCreateGallery
import kotlinx.android.synthetic.main.view_empty_selfies.btnImportPhotos
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.okButton
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

const val RC_PICK_IMAGE = 996
const val RC_CROP_IMAGE = 997
const val RC_IMAGE_CAPTURE = 998
const val RC_GOOGLE_SIGN_IN = 999

class SelfiesFragment : SimpleBaseFragment() {

    private val vmSelfies by viewModel<SelfiesViewModel>()
    private val vmAlbums by sharedViewModel<AlbumViewModel>()
    private val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()
    // need to initialize it lately, otherwise it would crash when changing a selfie-album
    private lateinit var selfiesAdapter: SelfiesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentSelfiesBinding>(
                inflater,
                R.layout.fragment_selfies,
                container,
                false
            )
        ) {
            vmSelfie = this@SelfiesFragment.vmSelfies
            vmGalleries = this@SelfiesFragment.vmAlbums
            lifecycleOwner = this@SelfiesFragment
            setStatusBarColor(R.color.status_bar_color_green)
            return root
        }
    }

    override fun onResume() {
        super.onResume()
        updateSelfieList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // View initialisation
        initRecyclerView()
        // if no photos are in the gallery
        btnImportPhotos.onClick {
            importPhotos()
        }
        // if no galleries are available
        btnCreateGallery.onClick {
            showAddGalleryDialog()
        }
        // bottombar
        showBottomBarNavigation()
        // fab
        fab.onClick {
            if (vmSelfies.isEditing().value == true) {
                if (vmGooglePhotos.alreadyLoggedIn()) {
                    removeFromGooglePhotosAlbum()
                }
                vmSelfies.deleteSelectedSelfies()
                vmSelfies.toggleEditing()
            } else {
                // take camera photo
                takeSelfieWithCamera()
            }
        }

        addViewModelObserver()
    }

    private fun invalidateMenu() {
        val menuResId = if (vmAlbums.isEmpty.value == true || vmSelfies.isEditing().value == true) {
            R.menu.empty_menu
        } else {
            R.menu.selfies
        }
        bottomAppBar.replaceMenu(menuResId)
        bottomAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_play -> {
                    context?.let {
                        if (hasVideoForAlbum(it, getAlbumId())) {
                            navigateToVideoFragment()
                        } else {
                            if (vmSelfies.hasEnoughSelfiesToCreateVideo()) {
                                navigateToCreateVideoFragment()
                            } else {
                                activity?.alert {
                                    message = getString(R.string.create_enough_selfies)
                                    okButton { dialog ->
                                        dialog.dismiss()
                                    }
                                }?.show()
                            }
                        }
                    }
                }
                R.id.action_delete_album -> {
                    showDeleteAlbumDialog()
                }
                R.id.action_import -> {
                    importPhotos()
                }
                R.id.action_preferences -> {
                    openPreferences()
                }
            }
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Timber.i("Successfully taken Selfie-Photo wit the Camera. Open now the Crop-View.")
            openCropActivity()
        } else if (requestCode == RC_CROP_IMAGE && resultCode == Activity.RESULT_OK) {
            val intentData = data ?: return
            val resultUri = UCrop.getOutput(intentData)
            Timber.i("Successfully cropped Selfie. Save it now to the Database and upload it if necessary to Google Photos. ${resultUri?.path}")
            resultUri?.path?.let {
                val selfieEntity = vmSelfies.addSelfieToAlbum(it)
                coroutineScope.launch(Dispatchers.IO) {
                    vmSelfies.detectFace(selfieEntity)
                    if (vmGooglePhotos.isUserSignedIn()) {
                        vmGooglePhotos.uploadSelfie(selfieEntity)
                    }
                }
                vmSelfies.refreshSelfies()
            }
        } else if (requestCode == RC_PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            importSelectedPhotos(data)
        }
    }
    //endregion Fragment Lifecycle

    private fun getAlbumId(): Long {
        context?.let {
            val preferencesHelper = PreferencesHelper(it)
            return preferencesHelper.lastSelectedSelfieAlbum
        }
        return 0
    }

    private fun updateSelfieList() {
        val albumId = getAlbumId()
        vmSelfies.albumId.postValue(albumId)
    }

    //region Action Handling -------------------------------------------------------------------------------------------------------------------------
    private fun navigateToVideoFragment() = findNavController().navigate(R.id.action_selfiesFragment_to_videoFragment)

    private fun navigateToCreateVideoFragment() =
        findNavController().navigate(R.id.action_selfiesFragment_to_faceTimeLapseFragment)

    private fun showAddGalleryDialog() {
        activity?.let {
            val viewInflated = LayoutInflater.from(context)
                .inflate(R.layout.view_gallery_name_dialog, view as ViewGroup?, false)
            val input = viewInflated.findViewById(R.id.input) as EditText
            AlertDialog.Builder(it)
                .setTitle(getString(R.string.album_name))
                .setView(viewInflated)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                    val albumName = input.text.toString()
                    val googlePhotosAlbumId = vmGooglePhotos.createAlbum(albumName)
                    vmAlbums.addAlbum(albumName, googlePhotosAlbumId)
                    vmSelfies.addAlbumToFirebaseUser(GooglePhotosAlbum(name = albumName, id = googlePhotosAlbumId))
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
                .show()
        }
    }

    private fun showEditAlbumDialog() {
        activity?.let {
            val viewInflated = LayoutInflater.from(context)
                .inflate(R.layout.view_gallery_name_dialog, view as ViewGroup?, false)
            val input = viewInflated.findViewById(R.id.input) as EditText
            input.setText(vmAlbums.getActiveAlbumName())
            AlertDialog.Builder(it)
                .setTitle(getString(R.string.album_name))
                .setView(viewInflated)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                    val galleryEntity = vmAlbums.getActiveAlbum()?.albumEntity
                    galleryEntity?.let {
                        val galleryName = input.text.toString()
                        galleryEntity.name = galleryName
                        vmAlbums.editAlbum(galleryEntity)
                    }
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
                .show()
        }
    }

    private fun showDeleteAlbumDialog() {
        activity?.let {
            AlertDialog.Builder(it)
                .setTitle(getString(R.string.dialog_delete_album_title))
                .setMessage(getString(R.string.dialog_delete_album_message))
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    val albumId = getAlbumId()
                    vmSelfies.deleteAlbumFromFirebaseUser(albumId)
                    vmSelfies.deletePhotosOfAlbum(albumId)
                    vmAlbums.peekNextAlbum()
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
                .show()
        }
    }

    private fun showGooglePhotosGallery() {
        activity?.let {
            val fragmentTransaction = it.supportFragmentManager.beginTransaction()
            val googlePhotosContainerFragment = newInstanceOf<GooglePhotosContainerFragment>()
            googlePhotosContainerFragment.show(fragmentTransaction, "GooglePhotosContainerFragment")
        }
    }

    private fun importPhotos() {
        val photoPickerIntent =
            Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        photoPickerIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, RC_PICK_IMAGE)
    }

    private fun openImageDetailView(imageView: ImageView, position: Int) {
        val context = context ?: return
        StfalconImageViewer.Builder<SelfiePhotoItemModel>(context, vmSelfies.getSelfiesForDetailedView()) { view, image ->
            val options = RequestOptions()
                .format(DecodeFormat.PREFER_RGB_565)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)

            Glide.with(context)
                .load(image.filePath)
                .thumbnail(0.4f)
                .apply(options)
                .into(view)
        }
            .withBackgroundColor(ContextCompat.getColor(imageView.context, R.color.background))
            .withStartPosition(position - 1)
            .withTransitionFrom(imageView)
            .show()
    }

    private fun openPreferences() {
        val intent = Intent(context, PrefsActivity::class.java)
        startActivity(intent)
    }

    private fun removeFromGooglePhotosAlbum() {
        vmSelfies.getSelectedSelfies()?.let { selfies ->
            selfies.forEach { selfie ->
                vmGooglePhotos.removeSelectedPhotosFromAlbum(selfie.googlePhotosId)
            }
        }
    }
    //endregion Action Handling

    private fun initRecyclerView() {
        with(rvSelfies) {
            val spanCount = 3
            val layoutManager = GridLayoutManager(context, spanCount)
            layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (selfiesAdapter.getItemViewType(position)) {
                        VIEW_TYPE_TITLE -> 3
                        VIEW_TYPE_PHOTO -> 1
                        else -> 0
                    }
                }
            }
            this.layoutManager = layoutManager
            isNestedScrollingEnabled = true

            selfiesAdapter = SelfiesAdapter(
                onSelfiePhotoClick = { imageView, position ->
                    onSelfiePhotoClick(imageView, position)
                },
                onSelfieLongClick = { position ->
                    vmSelfies.toggleEditing(position)
                }
            )

            adapter = selfiesAdapter

            val needToAddItemDecoration = itemDecorationCount == 0
            if (needToAddItemDecoration) {
                addItemDecoration(object : RecyclerView.ItemDecoration() {

                    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                        when (parent.getChildViewHolder(view)) {
                            is SelfieViewHolder -> {
                                outRect.top = 8
                                outRect.bottom = 8
                                outRect.left = 8
                                outRect.right = 8
                            }
                        }
                    }
                })
            }
        }
    }

    private fun addViewModelObserver() {
        with(vmSelfies) {
            // update when selfies changed
            selfiesPaged.observe(this@SelfiesFragment, Observer { pagedSelfies ->
                val pagedList = pagedSelfies ?: return@Observer
                selfiesAdapter.submitList(pagedList)
                invalidateMenu()
            })
            // update when edit-mode changes
            isEditing().observe(this@SelfiesFragment, Observer { isEditing ->
                // need to post this delayed, otherwise it isn't shown correctly
                val handler = Handler()
                handler.postDelayed({
                    setBottomBarNavigationIcon(if (isEditing) R.drawable.ic_close_24dp else R.drawable.ic_menu_24dp)
                    invalidateMenu()
                }, 100)

                val drawable = if (isEditing) {
                    R.drawable.ic_delete_24dp
                } else {
                    R.drawable.ic_add_black_24dp
                }
                fab.setImageDrawable(getDrawable(drawable))
            })
        }

        with(vmGooglePhotos) {
            userSignedIn.observe(this@SelfiesFragment, Observer { event ->
                event.getContentIfNotHandled()?.let {
                    showGooglePhotosGallery()
                }
            })
            isFinishedImporting.observe(this@SelfiesFragment, Observer { event ->
                event.getContentIfNotHandled()?.let {
                    updateSelfieList()
                }
            })
            googlePhotosApiIsReadyToUse.observe(viewLifecycleOwner, Observer { event ->
                event.getContentIfNotHandled()?.let {
                    syncPhotos()
                }
            })
        }

        with(vmAlbums) {
            isEmpty.observe(this@SelfiesFragment, Observer {
                invalidateMenu()
            })

            onCreateAlbumClicked.observe(this@SelfiesFragment, Observer { event ->
                event.getContentIfNotHandled()?.let {
                    showAddGalleryDialog()
                }
            })

            selectedAlbumId.observe(this@SelfiesFragment, Observer { event ->
                event.getContentIfNotHandled()?.let {
                    clearRecyclerView()
                    updateSelfieList()
                    syncWithGooglePhotosAlbum()
                }
            })
        }
    }

    private fun syncWithGooglePhotosAlbum() {
        if (vmGooglePhotos.alreadyLoggedIn()) {
            vmGooglePhotos.syncPhotos()
        }
    }

    private fun clearRecyclerView() = initRecyclerView()

    private fun getDrawable(drawableResId: Int): Drawable? {
        return context?.let {
            ContextCompat.getDrawable(it, drawableResId)
        }
    }

    private fun takeSelfieWithCamera() {
        val activity = activity ?: return
        Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            .apply {
                putExtra("android.intent.extras.CAMERA_FACING", android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT)
                putExtra("android.intent.extras.LENS_FACING_FRONT", 1)
                putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
            }
            .also { takePictureIntent ->
                // Ensure that there's a camera activity to handle the intent
                takePictureIntent.resolveActivity(activity.packageManager)?.also {
                    // Continue only if the File was successfully created
                    val photoURI: Uri = vmSelfies.getTempPhotoFile(activity)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    activity.startActivityForResult(takePictureIntent, RC_IMAGE_CAPTURE)
                }
            }
    }

    private fun openCropActivity() {
        val activity = activity ?: return
        val source = vmSelfies.getTempPhotoFile(activity)
        val destination = vmSelfies.getPhotoFilePath(activity)
        val options = UCrop.Options()
        options.withMaxResultSize(activity.getDisplayWidth().toInt() * 2, activity.getDisplayHeight().toInt() * 2)
        options.setBrightnessEnabled(true)
        options.setContrastEnabled(true)
        options.setSaturationEnabled(true)
        options.setSharpnessEnabled(true)
        options.setToolbarColor(ContextCompat.getColor(activity, R.color.primary))
        options.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorAccent))
        options.setActiveWidgetColor(ContextCompat.getColor(activity, R.color.colorAccent))
        options.setToolbarWidgetColor(ContextCompat.getColor(activity, R.color.white))
        UCrop.of(source, Uri.fromFile(destination))
            .useSourceImageAspectRatio()
            .withOptions(options)
            .start(activity, RC_CROP_IMAGE)
    }

    private fun importSelectedPhotos(intent: Intent?) {
        intent?.clipData?.let { data ->
            val uris = mutableListOf<Uri>()
            for (i in 0 until data.itemCount) {
                val image = data.getItemAt(i).uri
                uris.add(image)
            }

            val progressDialog = activity?.indeterminateProgressDialog(
                title = getString(R.string.dialog_import_title)
            )
            progressDialog?.setCancelable(false)
            progressDialog?.show()
            coroutineScope.launch(Dispatchers.IO) {
                val selfiesToUpload = vmSelfies.importPhotosFromDevice(uris)
                activity?.runOnUiThread {
                    progressDialog?.hide()
                }
                vmSelfies.refreshSelfies()
                if (vmGooglePhotos.isUserSignedIn()) {
                    selfiesToUpload.forEach { selfie ->
                        vmSelfies.detectFace(selfie)
                        vmGooglePhotos.uploadSelfie(selfie)
                    }
                }
            }
        }
    }

    private fun onSelfiePhotoClick(imageView: ImageView, position: Int) {
        if (vmSelfies.isEditing().value == true) {
            val selfie = vmSelfies.getSelfieForPosition(position)
            selfie?.isSelected?.set(selfie.isSelected.get().not())
        } else {
            openImageDetailView(imageView, position)
        }
    }

    //region Bottom App Bar --------------------------------------------------------------------------------------------------------------------------
    private fun showBottomBarNavigation() {
        bottomAppBar.setNavigationIcon(R.drawable.ic_menu_24dp)
        bottomAppBar.setNavigationOnClickListener {
            if (vmSelfies.isEditing().value == true) {
                vmSelfies.toggleEditing()
            } else {
                val activity = activity ?: return@setNavigationOnClickListener
                val bottomNavDrawerFragment = BottomNavigationDrawerFragment()
                bottomNavDrawerFragment.show(activity.supportFragmentManager, bottomNavDrawerFragment.tag)
            }
        }
    }

    private fun setBottomBarNavigationIcon(drawableResId: Int) {
        bottomAppBar.setNavigationIcon(drawableResId)
    }
    //endregion Bottom App Bar

}