package de.sieber.dailyselfie.ui.model

data class GooglePhotoAlbumModel(
    val id: String = "",
    val thumbnailUrl: String = "",
    val title: String = ""
)