package de.sieber.dailyselfie.ui.google.paging

import android.content.Context
import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.photos.library.v1.PhotosLibraryClient
import com.google.photos.library.v1.proto.BatchRemoveMediaItemsFromAlbumRequest
import com.google.photos.library.v1.proto.SearchMediaItemsRequest
import com.google.photos.types.proto.MediaItem
import com.tonyodev.fetch2.BuildConfig
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2.Error
import com.tonyodev.fetch2.Fetch
import com.tonyodev.fetch2.FetchConfiguration
import com.tonyodev.fetch2.FetchListener
import com.tonyodev.fetch2.NetworkType
import com.tonyodev.fetch2.Priority
import com.tonyodev.fetch2.Request
import com.tonyodev.fetch2core.DownloadBlock
import com.tonyodev.fetch2core.Extras
import de.sieber.dailyselfie.database.dao.SelfieDao
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.ScopedClass
import de.sieber.dailyselfie.ui.model.GooglePhotoAlbumModel
import de.sieber.dailyselfie.ui.model.GooglePhotoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

const val creationTimeExtrasKey = "creationTimeInSeconds"
const val googlePhotosIdExtraKey = "googlePhotosId"
const val localAlbumIdKey = "localAlbumId"

class GooglePhotosRepository(
    private val context: Context,
    private val googlePhotosLibraryClient: PhotosLibraryClient,
    private val selfieDao: SelfieDao,
    private val albumRepository: AlbumRepository
) : ScopedClass(Dispatchers.Main) {

    private val fetch by lazy {
        val fetchConfiguration = FetchConfiguration.Builder(context)
            .setDownloadConcurrentLimit(20)
            .enableLogging(BuildConfig.DEBUG)
            .build()
        Fetch.getInstance(fetchConfiguration)
    }

    @MainThread
    fun getGooglePhotos(): GooglePhotosListing {
        // create source
        val sourceFactory = GooglePhotosDataFactory(googlePhotosLibraryClient)

        // setup page-configuration
        val pageSize = 50
        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(pageSize)
            .setPageSize(pageSize)
            .build()
        val livePagedList = LivePagedListBuilder(sourceFactory, pagedListConfig).build()

        // create Listing-Model for Observing on UI
        return GooglePhotosListing(
            isInitialLoading = Transformations.switchMap(sourceFactory.sourceLiveData) {
                it.isInitialLoading()
            },
            isPaging = Transformations.switchMap(sourceFactory.sourceLiveData) {
                it.isPaging()
            },
            pagedList = livePagedList
        )
    }

    fun createAlbum(albumTitle: String): String {
        val album = googlePhotosLibraryClient.createAlbum(albumTitle)
        return album.id
    }

    fun listAlbumIds(): List<GooglePhotoAlbumModel> {
        val albums = googlePhotosLibraryClient.listAlbums()
        return albums.page.response.albumsList.map {
            GooglePhotoAlbumModel(
                id = it.id,
                thumbnailUrl = it.coverPhotoBaseUrl,
                title = it.title
            )
        }
    }

    fun existsAlbum(id: String): Boolean {
        return if (id.isEmpty()) {
            false
        } else {
            googlePhotosLibraryClient.getAlbum(id) != null
        }
    }

    fun listPhotosOfAlbum(albumId: String): List<MediaItem> {
        val pageSize = 100
        val returnValue = mutableListOf<MediaItem>()
        val request = SearchMediaItemsRequest.newBuilder()
            .setAlbumId(albumId)
            .setPageSize(pageSize)
            .build()
        val response = googlePhotosLibraryClient.searchMediaItemsCallable().call(request)
        returnValue.addAll(response.mediaItemsList)
        var nextPageToken = response.nextPageToken
        while (nextPageToken.isNotEmpty()) {
            val nextRequest = SearchMediaItemsRequest.newBuilder()
                .setAlbumId(albumId)
                .setPageSize(pageSize)
                .setPageToken(nextPageToken)
                .build()
            val nextResponse = googlePhotosLibraryClient.searchMediaItemsCallable().call(nextRequest)
            returnValue.addAll(nextResponse.mediaItemsList)
            nextPageToken = nextResponse.nextPageToken
        }

        return returnValue
    }

    suspend fun downloadPhoto(
        context: Context,
        googlePhotoModels: List<GooglePhotoModel>,
        downloadProgress: (downloaded: Int, totalDownload: Int) -> Unit
    ) {
        lateinit var result: Continuation<Unit>

        val downloadMap = mutableMapOf<String, GooglePhotoModel>()
        var countOfDownloads = 0
        fetch.addListener(object : PhotoFetchListener() {

            override fun onCompleted(download: Download) {
                super.onCompleted(download)
                ++countOfDownloads

                downloadMap[download.url]?.let {
                    downloadMap.remove(download.url)

                    val creationTimeInSeconds = download.extras.map[creationTimeExtrasKey]?.toLong() ?: 0
                    val googlePhotoId = download.extras.map[googlePhotosIdExtraKey] ?: ""
                    val localAlbumId = download.extras.map[localAlbumIdKey]?.toLong() ?: 0
                    val selfie = SelfieEntity(
                        albumId = localAlbumId,
                        filePath = download.file,
                        creationTimeInSeconds = creationTimeInSeconds,
                        googlePhotosId = googlePhotoId
                    )
                    selfieDao.insertSelfieEntity(selfie)

                    GlobalScope.launch(Dispatchers.IO) {
                        selfie.detectFace(albumRepository)
                    }

                    downloadProgress(googlePhotoModels.size - downloadMap.size, googlePhotoModels.size)

                    if (downloadMap.isEmpty()) {
                        result.resume(Unit)
                    }
                }
            }
        })

        val dataDir = context.filesDir.absolutePath
        val temp = mutableListOf<GooglePhotoModel>()
        googlePhotoModels
            .map { googlePhoto ->
                val url = googlePhoto.largeImageUrl
                val file = "$dataDir/${googlePhoto.fileName}"
                val request = Request(url, file)

                val s = temp.find { it.fileName == googlePhoto.fileName }
                if (s != null) {
                    Timber.i(googlePhoto.toString())
                    Timber.i(s.toString())
                }
                temp.add(googlePhoto)


                request.extras = getRequestExtras(googlePhoto)
                downloadMap[url] = googlePhoto

                fetch.enqueue(
                    request = request
                )
//            Timber.i("Download Start: $request")
            }
        return suspendCoroutine { continuation -> result = continuation }
    }

    private fun getRequestExtras(googlePhotoModel: GooglePhotoModel): Extras {
        val creationTime = Pair(creationTimeExtrasKey, googlePhotoModel.creationTimeInSeconds.toString())
        val googlePhotosId = Pair(googlePhotosIdExtraKey, googlePhotoModel.id)
        val localAlbumId = Pair(localAlbumIdKey, googlePhotoModel.localAlbumId.toString())
        return Extras(mapOf(creationTime, googlePhotosId, localAlbumId))
    }

    fun downloadPhoto(
        context: Context,
        googlePhotoModels: List<GooglePhotoModel>,
        isImporting: MutableLiveData<Boolean>,
        finished: MutableLiveData<Event<Boolean>>,
        albumId: Long
    ) {
        launch(Dispatchers.IO) {
            isImporting.postValue(true)

            val downloadMap = mutableMapOf<Int, GooglePhotoModel>()
            fetch.addListener(object : PhotoFetchListener() {

                override fun onCompleted(download: Download) {
                    super.onCompleted(download)

                    downloadMap[download.id]?.let {
                        downloadMap.remove(download.id)

                        val creationTimeInSeconds = download.extras.map[creationTimeExtrasKey]?.toLong() ?: 0
                        val googlePhotoId = download.extras.map[googlePhotosIdExtraKey] ?: ""
                        val selfie = SelfieEntity(
                            albumId = albumId,
                            filePath = download.file,
                            creationTimeInSeconds = creationTimeInSeconds,
                            googlePhotosId = googlePhotoId
                        )
                        selfieDao.insertSelfieEntity(selfie)

                        if (downloadMap.isEmpty()) {
                            isImporting.postValue(false)
                            finished.postValue(Event(true))
                        }
                    }
                }
            })

            val dataDir = context.filesDir.absolutePath
            googlePhotoModels.forEach { googlePhoto ->
                val url = googlePhoto.largeImageUrl
                val file = "$dataDir/${googlePhoto.fileName}"

                val request = Request(url, file)
                    .apply {
                        priority = Priority.HIGH
                        networkType = NetworkType.ALL
                    }
                val creationTime = Pair(creationTimeExtrasKey, googlePhoto.creationTimeInSeconds.toString())
                val googlePhotosId = Pair(googlePhotosIdExtraKey, googlePhoto.id)
                request.extras = Extras(mapOf(creationTime, googlePhotosId))

                downloadMap[request.id] = googlePhoto

                fetch.enqueue(request = request)
            }
        }
    }

    fun removePhotoFromGooglePhotosAlbum(googlePhotosAlbumId: String, googlePhotosId: String) {
        Timber.i("Remove Photo: $googlePhotosId from Album $googlePhotosAlbumId")
        val request = BatchRemoveMediaItemsFromAlbumRequest.newBuilder()
            .setAlbumId(googlePhotosAlbumId)
            .addMediaItemIds(googlePhotosId)
            .build()

        try {
            googlePhotosLibraryClient.batchRemoveMediaItemsFromAlbum(request)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    private open class PhotoFetchListener : FetchListener {

        override fun onAdded(download: Download) = Unit

        override fun onCancelled(download: Download) = Unit

        override fun onCompleted(download: Download) = Unit

        override fun onDeleted(download: Download) = Unit

        override fun onDownloadBlockUpdated(download: Download, downloadBlock: DownloadBlock, totalBlocks: Int) = Unit

        override fun onError(download: Download, error: Error, throwable: Throwable?) = Unit

        override fun onPaused(download: Download) = Unit

        override fun onProgress(download: Download, etaInMilliSeconds: Long, downloadedBytesPerSecond: Long) = Unit

        override fun onQueued(download: Download, waitingOnNetwork: Boolean) = Unit

        override fun onRemoved(download: Download) = Unit

        override fun onResumed(download: Download) = Unit

        override fun onStarted(download: Download, downloadBlocks: List<DownloadBlock>, totalBlocks: Int) = Unit

        override fun onWaitingNetwork(download: Download) = Unit

    }
}
