package de.sieber.dailyselfie.ui.common.fragment

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarManager
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarModel


abstract class SimpleBaseFragment : Fragment() {

    protected val toolbarModel: ToolbarModel by lazy {
        createToolbar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(this, view, toolbarModel).prepareToolbar()
    }

    protected open fun createToolbar(): ToolbarModel {
        return ToolbarModel.Builder()
            .build()
    }

    fun setStatusBarColor(@ColorRes color: Int) {
        activity?.window?.let { window ->
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(context!!, color)
        }
    }
}