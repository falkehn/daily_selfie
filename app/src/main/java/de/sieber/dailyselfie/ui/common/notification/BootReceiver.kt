package de.sieber.dailyselfie.ui.common.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.sieber.dailyselfie.ui.common.NotificationUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper

class BootReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            val notificationUtils = NotificationUtils(context)
            val prefHelper = PreferencesHelper(context)
            val reminderTime = prefHelper.notificationsTime
            notificationUtils.createRepeatingAlarmManager(reminderTime)
        }
    }

}

