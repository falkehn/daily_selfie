package de.sieber.dailyselfie.ui.google.fragment

import android.content.Intent
import android.os.Bundle
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseFragment
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.selfies.RC_GOOGLE_SIGN_IN
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import timber.log.Timber

open class SimpleGooglePhotosBaseFragment : SimpleBaseFragment() {

    protected val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (vmGooglePhotos.hasValidAuthToken()) {
            vmGooglePhotos.initializePhotoLibraryClient()
        }
        vmGooglePhotos.isFinishedImporting.value = Event(false)
    }

    override fun onStart() {
        super.onStart()

        if (vmGooglePhotos.hasValidAuthToken().not()) {
            updateAuthToken()
        }
    }
    //endregion Fragment Lifecycle

    private fun updateAuthToken() {
        vmGooglePhotos.createGoogleApiClient(activity!!)
        val intent = vmGooglePhotos.getSignInIntent()
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            data?.let {
                vmGooglePhotos.handleSignInResult(
                    data = it,
                    onSuccess = {
                        vmGooglePhotos.initializePhotoLibraryClient()
                    },
                    onFailure = {
                        Timber.e("An error occurred.")
                    }
                )

            }
        }
    }

}