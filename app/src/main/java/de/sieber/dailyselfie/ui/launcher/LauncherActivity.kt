package de.sieber.dailyselfie.ui.launcher

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import de.sieber.dailyselfie.ui.main.MainActivity
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.onboarding.OnBoardingActivity
import org.koin.android.ext.android.inject

class LauncherActivity : FragmentActivity() {

    private val preferencesHelper by inject<PreferencesHelper>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val onBoardingFinished = preferencesHelper.onBoardingFinished
        val intent = if (onBoardingFinished) {
            Intent(this, MainActivity::class.java)
        } else {
            Intent(this, OnBoardingActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}