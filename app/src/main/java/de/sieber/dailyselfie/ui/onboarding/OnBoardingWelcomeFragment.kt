package de.sieber.dailyselfie.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentOnboardingWelcomeBinding

class OnBoardingWelcomeFragment : BaseOnBoardingFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentOnboardingWelcomeBinding>(
                inflater,
                R.layout.fragment_onboarding_welcome,
                container,
                false
            )
        ) {
            return root
        }
    }

    override fun playAnimation() = Unit

}