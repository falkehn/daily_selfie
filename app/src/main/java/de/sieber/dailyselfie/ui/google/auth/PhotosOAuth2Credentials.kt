package de.sieber.dailyselfie.ui.google.auth

import com.google.auth.oauth2.AccessToken
import com.google.auth.oauth2.OAuth2Credentials
import de.sieber.dailyselfie.database.repository.OAuthCredentialsRepository
import java.util.*

class PhotosOAuth2Credentials(
    private val oAuthCredentialsRepository: OAuthCredentialsRepository
) : OAuth2Credentials() {

    override fun refreshAccessToken(): AccessToken {
        oAuthCredentialsRepository.getOAuthCredentials()?.let {
            return AccessToken(
                it.accessToken,
                Date(it.expiresIn)
            )
        }
        throw IllegalStateException("No Access Token in Repository!")
    }

}