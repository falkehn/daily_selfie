package de.sieber.dailyselfie.ui.galleries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_menu.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class BottomNavigationDrawerFragment : BottomSheetDialogFragment() {

    private val vmAlbums by sharedViewModel<AlbumViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val albumAdapter = AlbumAdapter { album ->
            context?.let {
                val preferencesHelper = PreferencesHelper(it)
                preferencesHelper.lastSelectedSelfieAlbum = album.id
                vmAlbums.selectedAlbumId.postValue(Event(album.id))
                this@BottomNavigationDrawerFragment.dismiss()
            }
        }
        with(rvAlbums) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = albumAdapter
        }

        vmAlbums.albums.observe(this, Observer { albums ->
            albums?.let {
                albumAdapter.setGalleries(albums)
            }
        })

        tvCreateNewGallery.onClick {
            vmAlbums.onCreateAlbumClicked.postValue(Event(Any()))
            this@BottomNavigationDrawerFragment.dismiss()
        }
    }
    //endregion Fragment-Lifecycle

}