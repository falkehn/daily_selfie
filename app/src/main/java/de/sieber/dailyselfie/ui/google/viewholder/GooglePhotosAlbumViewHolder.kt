package de.sieber.dailyselfie.ui.google.viewholder

import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.databinding.ItemGooglePhotoAlbumBinding
import de.sieber.dailyselfie.ui.model.GooglePhotoAlbumModel
import org.jetbrains.anko.sdk25.coroutines.onClick

class GooglePhotosAlbumViewHolder(
    private val binding: ItemGooglePhotoAlbumBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(googlePhotoAlbumModel: GooglePhotoAlbumModel, onGooglePhotoAlbumClick: (googlePhotoAlbumModel: GooglePhotoAlbumModel) -> Unit) {
        with(binding) {
            this.album = googlePhotoAlbumModel
            this.executePendingBindings()
            this.root.onClick {
                onGooglePhotoAlbumClick(googlePhotoAlbumModel)
            }
        }
    }

}