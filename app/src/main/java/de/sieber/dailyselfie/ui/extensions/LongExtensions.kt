package de.sieber.dailyselfie.ui.extensions

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

fun Long.getTimeString(isInSeconds: Boolean = false): String {
    val calendar = Calendar.getInstance()
    if (isInSeconds) {
        calendar.timeInMillis = this * 1000L
    } else {
        calendar.timeInMillis = this
    }
    val simpleDateFormatter = SimpleDateFormat("dd/MM/yy", Locale.getDefault())
    return simpleDateFormatter.format(calendar.time)
}