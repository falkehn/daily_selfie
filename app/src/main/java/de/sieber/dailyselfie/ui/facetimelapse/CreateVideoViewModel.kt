package de.sieber.dailyselfie.ui.facetimelapse

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.extensions.getDisplayHeight
import de.sieber.dailyselfie.ui.extensions.getDisplayWidth
import de.sieber.dailyselfie.ui.extensions.getTimeString
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Calendar
import java.util.Date


enum class Speed {
    Slow,
    Normal,
    Quick,
    Fast
}

class CreateVideoViewModel(
    val application: Application,
    private val albumRepository: AlbumRepository
) : ScopedViewModel(Dispatchers.IO) {

    private val selfieEntities = MutableLiveData<List<SelfieEntity>>()

    val useAllPhotos = MutableLiveData<Boolean>().apply {
        postValue(true)
    }

    val availableTimeRangeFrom = MutableLiveData<Calendar>()
    val availableTimeRangeTill = MutableLiveData<Calendar>()

    val selectedTimeRangeFrom = MutableLiveData<Calendar>()
    val selectedTimeRangeTill = MutableLiveData<Calendar>()

    val timeRangeFromAsString: LiveData<String> = Transformations.map(selectedTimeRangeFrom) {
        "${application.getString(R.string.from)} ${it.timeInMillis.getTimeString()}"
    }
    val timeRangeTillAsString: LiveData<String> = Transformations.map(selectedTimeRangeTill) {
        "${application.getString(R.string.from)} ${it.timeInMillis.getTimeString()}"
    }
    var selectedSpeed = Speed.Normal
    var showDates = true

    private val finishedVideoCreation = MutableLiveData<Event<Boolean>>()

    init {
        launch {
            val albumId = getAlbumId()
            val selfies = albumRepository.getSelfieEntitiesForAlbumIdAsc(albumId)
            selfieEntities.postValue(selfies)

            val from = Calendar.getInstance()
            from.time = getDateOfFirstSelfie(selfies)
            availableTimeRangeFrom.postValue(from)

            val till = Calendar.getInstance()
            till.time = getDateOfLastSelfie(selfies)
            availableTimeRangeTill.postValue(till)

            selectedTimeRangeFrom.postValue(from)
            selectedTimeRangeTill.postValue(till)
        }
    }

    private fun getAlbumId(): Long {
        val preferencesHelper = PreferencesHelper(application)
        return preferencesHelper.lastSelectedSelfieAlbum
    }

    fun getFramesPerSecond(): Int =
        when (selectedSpeed) {
            Speed.Slow -> 2
            Speed.Normal -> 5
            Speed.Quick -> 10
            Speed.Fast -> 24
        }

    fun getTimeRangeFromInSeconds() = selectedTimeRangeFrom.value?.timeInMillis?.div(1000) ?: 0L

    fun getTimeRangeTillInSeconds() = selectedTimeRangeTill.value?.timeInMillis?.div(1000) ?: 0L

    fun getVideoWidth() = application.getDisplayWidth()

    fun getVideoHeight() = application.getDisplayHeight()

    fun getVideoFaceMaxWidth() = 0.7f * getVideoWidth()

    private fun getDateOfFirstSelfie(selfies: List<SelfieEntity>): Date {
        val creationTimeInSeconds = selfies.first().creationTimeInSeconds * 1000
        return Date(creationTimeInSeconds)
    }

    private fun getDateOfLastSelfie(selfies: List<SelfieEntity>): Date {
        val creationTimeInSeconds = selfies.last().creationTimeInSeconds * 1000
        return Date(creationTimeInSeconds)
    }

    fun hasFinishedVideoCreation(): LiveData<Event<Boolean>> = finishedVideoCreation

}
