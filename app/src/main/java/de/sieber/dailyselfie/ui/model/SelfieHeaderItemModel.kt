package de.sieber.dailyselfie.ui.model

data class SelfieHeaderItemModel(
    val title: String = "",
    val description: String = ""
) : SelfieItemBaseModel()