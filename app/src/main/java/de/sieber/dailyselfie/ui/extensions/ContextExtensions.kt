package de.sieber.dailyselfie.ui.extensions

import android.content.Context
import android.graphics.Point
import android.view.WindowManager


fun Context.getDisplayWidth(): Float {
    val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = wm.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.x.toFloat()
}

fun Context.getDisplayHeight(): Float {
    val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    val display = wm.defaultDisplay
    val size = Point()
    display.getSize(size)
    return size.y.toFloat()
}
