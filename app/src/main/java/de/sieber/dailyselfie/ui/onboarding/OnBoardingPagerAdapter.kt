package de.sieber.dailyselfie.ui.onboarding

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class OnBoardingPagerAdapter(
    supportedFragmentManager: FragmentManager,
    private val useLoginScreen: Boolean
) : FragmentStatePagerAdapter(supportedFragmentManager) {

    private var fragmentListWithLogin = mutableListOf(
        OnBoardingWelcomeFragment(),
        OnBoardingLoginFragment(),
        OnBoardingNotificationFragment(),
        OnBoardingCreateAlbumFragment()
    )

    private var fragmentListWithoutLogin = mutableListOf(
        OnBoardingWelcomeFragment(),
        OnBoardingNotificationFragment(),
        OnBoardingCreateAlbumFragment()
    )

    // Returns total number of pages
    override fun getCount(): Int {
        return if (useLoginScreen) fragmentListWithLogin.size else fragmentListWithoutLogin.size
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment {
        return if (useLoginScreen) fragmentListWithLogin[position] else fragmentListWithoutLogin[position]
    }

}