package de.sieber.dailyselfie.ui.common

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import de.sieber.dailyselfie.ui.main.EXTRA_OPEN_VIDEO
import de.sieber.dailyselfie.ui.main.MainActivity

object IntentUtils {

    fun getMainActivityPendingIntent(context: Context, albumId: Long = -1, hashCode: Int): PendingIntent {
        val resultIntent = Intent(context, MainActivity::class.java)
        if (albumId != -1L) {
            resultIntent.putExtra(EXTRA_OPEN_VIDEO, albumId)
        }
        resultIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        return PendingIntent.getActivity(
            context,
            hashCode,
            resultIntent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

}