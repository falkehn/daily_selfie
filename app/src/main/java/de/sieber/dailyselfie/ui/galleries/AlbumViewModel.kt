package de.sieber.dailyselfie.ui.galleries

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import de.sieber.dailyselfie.database.entities.AlbumEntity
import de.sieber.dailyselfie.database.relation.AlbumWithSelfies
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.model.GalleryModel
import kotlinx.coroutines.Dispatchers

class AlbumViewModel(
    private val preferencesHelper: PreferencesHelper,
    private val albumRepository: AlbumRepository
) : ScopedViewModel(Dispatchers.IO) {

    val albums: LiveData<List<GalleryModel>> =
        Transformations.map(albumRepository.getAlbumsWithSelfiesAsLiveData()) { galleries ->
            galleries.map {
                GalleryModel(
                    id = it.albumEntity?.id ?: 0,
                    name = "${it.albumEntity?.name} (${it.selfies?.count()})",
                    timeRange = it.albumEntity?.timeRange ?: "",
                    countOfSelfies = it.selfies?.count() ?: 0
                )
            }
        }

    val isEmpty: LiveData<Boolean> = Transformations.map(albums) { galleries ->
        galleries.isEmpty()
    }

    val selectedAlbumId = MutableLiveData<Event<Long>>()

    val onCreateAlbumClicked = MutableLiveData<Event<Any>>()

    fun peekNextAlbum() {
        if (albumRepository.getAlbumsWithSelfies().isNotEmpty()) {
            val nextAlbum = albumRepository.getAlbumsWithSelfies().first().albumEntity
            preferencesHelper.lastSelectedSelfieAlbum = nextAlbum?.id ?: 0
            nextAlbum?.let {
                selectedAlbumId.postValue(Event(it.id))
            }

        }
    }

    fun addAlbum(name: String, googlePhotosAlbumId: String) {
        val galleryEntity = AlbumEntity(
            name = name,
            googlePhotosAlbumId = googlePhotosAlbumId
        )
        val id = albumRepository.addAlbum(galleryEntity)
        preferencesHelper.lastSelectedSelfieAlbum = id
        selectedAlbumId.postValue(Event(id))
    }

    fun editAlbum(albumEntity: AlbumEntity) {
        albumRepository.updateAlbum(albumEntity)
//        val test = albumRepository.getSelfiesOfAlbumAsLiveData(id)
        preferencesHelper.lastSelectedSelfieAlbum = albumEntity.id
        selectedAlbumId.postValue(Event(albumEntity.id))
    }

    fun deleteAlbum(id: Long) {
        albumRepository.deleteAlbumWithSelfies(id)
    }

    fun getActiveAlbum(): AlbumWithSelfies? {
        val galleryId = preferencesHelper.lastSelectedSelfieAlbum
        return albumRepository.getAlbumWithSelfies(galleryId)
    }

    fun getActiveAlbumName(): String {
        val galleryId = preferencesHelper.lastSelectedSelfieAlbum
        return albumRepository.getAlbumWithSelfies(galleryId)?.albumEntity?.name ?: ""
    }

}