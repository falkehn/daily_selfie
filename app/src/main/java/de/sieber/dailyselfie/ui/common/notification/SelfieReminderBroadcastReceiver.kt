package de.sieber.dailyselfie.ui.common.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.common.IntentUtils
import de.sieber.dailyselfie.ui.common.NotificationUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.main.MainActivity
import org.koin.standalone.KoinComponent

/**
 * Receiver which gets triggered, when a Product-Reminder-Alarm has been fired.
 */
class SelfieReminderBroadcastReceiver : BroadcastReceiver(), KoinComponent {

    override fun onReceive(context: Context, intent: Intent) {
        val preferencesHelper = PreferencesHelper(context)
        if (preferencesHelper.notificationsEnabled) {
            showNotification(
                context,
                context.getString(R.string.notification_reminder_title),
                context.getString(R.string.notification_reminder_description)
            )
        }
        setupNextNotification(context)
    }

    private fun setupNextNotification(context: Context) {
        val notificationUtils = NotificationUtils(context)
        val prefHelper = PreferencesHelper(context)
        val reminderTime = prefHelper.notificationsTime
        notificationUtils.createRepeatingAlarmManager(reminderTime)
    }

    private fun showNotification(
        context: Context,
        title: String,
        text: String
    ) {
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val brandPrimaryColor = ContextCompat.getColor(context, R.color.primary)
        val tickerText = title + "\n" + text
        val resultIntent = Intent(context, MainActivity::class.java)
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val resultPendingIntent =
            IntentUtils.getMainActivityPendingIntent(context = context, hashCode = tickerText.hashCode())

        val channelId = "channel_01"

        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setContentTitle(title)
            .setContentText(text)
            .setTicker(tickerText)
            .setStyle(NotificationCompat.BigTextStyle().bigText(text))
            .setSmallIcon(R.drawable.ic_stat_name)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setColor(brandPrimaryColor)
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setLights(brandPrimaryColor, 5000, 5000)
            .setContentIntent(resultPendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, title, NotificationManager.IMPORTANCE_DEFAULT)
            with(channel) {
                lightColor = brandPrimaryColor
                enableLights(true)
                enableVibration(true)
                setShowBadge(true)
                notificationManager.createNotificationChannel(this)
            }
            notificationBuilder.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
        }

        val notification = notificationBuilder.build()
        notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL

        val notifyId = text.hashCode()
        notificationManager.notify(notifyId, notification)
    }

}