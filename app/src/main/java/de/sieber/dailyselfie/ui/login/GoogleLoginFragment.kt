package de.sieber.dailyselfie.ui.login

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentGoogleLoginBinding
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseDialogFragment
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.selfies.RC_GOOGLE_SIGN_IN
import kotlinx.android.synthetic.main.fragment_google_login.btnDownloadFromPlayStore
import kotlinx.android.synthetic.main.fragment_google_login.btnSignIn
import kotlinx.android.synthetic.main.fragment_google_login.ivClose
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class GoogleLoginFragment : SimpleBaseDialogFragment() {

    private val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentGoogleLoginBinding>(
                inflater,
                R.layout.fragment_google_login,
                container,
                false
            )
        ) {

            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivClose.onClick {
            dismiss()
        }

        btnSignIn.onClick {
            signIn()
        }

        btnDownloadFromPlayStore.onClick {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.apps.photos")
                )
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_GOOGLE_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            data?.let {
                vmGooglePhotos.handleSignInResult(
                    data = it,
                    onSuccess = {
                        vmGooglePhotos.userSignedIn.postValue(Event(true))
                        dismiss()
                    },
                    onFailure = {
                        vmGooglePhotos.userSignedIn.postValue(Event(false))
                        dismiss()
                    }
                )

            }
        }
    }
    //endregion Fragment-Lifecycle

    private fun signIn() {
        val intent = vmGooglePhotos.getSignInIntent()
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN)
    }

}