package de.sieber.dailyselfie.ui.selfies

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.model.SelfieHeaderItemModel
import de.sieber.dailyselfie.ui.model.SelfieItemBaseModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel
import org.koin.standalone.KoinComponent

const val VIEW_TYPE_TITLE = 0
const val VIEW_TYPE_PHOTO = 1
const val VIEW_TYPE_UNKNOWN = 2

class SelfiesAdapter(
    private val onSelfiePhotoClick: (imageView: ImageView, positionInList: Int) -> Unit,
    private val onSelfieLongClick: (positionInList: Int) -> Unit
) : PagedListAdapter<SelfieItemBaseModel, SelfieViewHolder>(SELFIE_COMPARATOR), KoinComponent {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelfieViewHolder =
        when (viewType) {
            VIEW_TYPE_TITLE -> {
                SelfieViewHolder.SelfiesHeaderViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_selfie_header,
                        parent,
                        false
                    )
                )
            }

            VIEW_TYPE_PHOTO -> {
                SelfieViewHolder.SelfieItemViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_selfie_photo,
                        parent,
                        false
                    )
                )
            }
            else -> throw IllegalStateException("")
        }


    override fun onBindViewHolder(holderItem: SelfieViewHolder, position: Int) {
        when (holderItem) {
            is SelfieViewHolder.SelfieItemViewHolder -> {
                val selfieItem = getItem(position) as SelfiePhotoItemModel
                holderItem.bind(selfieItem, onSelfiePhotoClick, onSelfieLongClick)
            }
            is SelfieViewHolder.SelfiesHeaderViewHolder -> {
                val header = getItem(position) as SelfieHeaderItemModel
                holderItem.bind(header)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is SelfiePhotoItemModel -> VIEW_TYPE_PHOTO
            is SelfieHeaderItemModel -> VIEW_TYPE_TITLE
            else -> VIEW_TYPE_UNKNOWN
        }
    }

    companion object {

        private val SELFIE_COMPARATOR = object : DiffUtil.ItemCallback<SelfieItemBaseModel>() {

            override fun areItemsTheSame(oldItem: SelfieItemBaseModel, newItem: SelfieItemBaseModel): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }

            override fun areContentsTheSame(oldItem: SelfieItemBaseModel, newItem: SelfieItemBaseModel): Boolean {
                return oldItem == newItem
            }

        }

    }

}