package de.sieber.dailyselfie.ui.google.paging

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.ui.google.viewholder.GooglePhotosViewHolder
import de.sieber.dailyselfie.ui.model.GooglePhotoModel

class GooglePhotosAdapter(
    private val onGooglePhotoClick: (googlePhotoModel: GooglePhotoModel) -> Unit
) : PagedListAdapter<GooglePhotoModel, GooglePhotosViewHolder>(GOOGLE_PHOTOS_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GooglePhotosViewHolder =
        GooglePhotosViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_google_photo,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: GooglePhotosViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it, onGooglePhotoClick)
        }
    }

    companion object {

        private val GOOGLE_PHOTOS_COMPARATOR = object : DiffUtil.ItemCallback<GooglePhotoModel>() {

            override fun areItemsTheSame(oldItem: GooglePhotoModel, newItem: GooglePhotoModel): Boolean {
                return oldItem.hashCode() == newItem.hashCode()
            }

            override fun areContentsTheSame(oldItem: GooglePhotoModel, newItem: GooglePhotoModel): Boolean {
                return oldItem == newItem
            }

        }

    }

}