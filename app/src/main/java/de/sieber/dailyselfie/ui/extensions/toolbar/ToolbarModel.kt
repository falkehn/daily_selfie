package de.sieber.dailyselfie.ui.extensions.toolbar

import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.MenuRes
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData

/**
 * A Data-Model to represent the Toolbar. Use the [ToolbarModel.Builder] class to create a [ToolbarModel] instance.
 */
data class ToolbarModel(
    @IdRes val resId: Int,
    val title: MutableLiveData<String> = MutableLiveData(),
    val show: Boolean,
    val hasNavigationButton: Boolean,
    val backButtonClickListener: ((view: View) -> Unit)?,
    val scrollBehavior: ToolbarScrollBehavior = ToolbarScrollBehavior.SCROLL_OUT,
    val menuResId: MutableLiveData<Int> = MutableLiveData(),
    val menuItemClickListener: ((menu: MenuItem) -> Boolean)?
) {

    companion object {

        const val NO_TOOLBAR = 0
        const val PARAMETER_NOT_SET = 0

    }

    /**
     * An Utility class to create a [ToolbarModel] using the Builder Pattern.
     */
    class Builder {

        private var resId: Int = NO_TOOLBAR
        private var title: MutableLiveData<String> = MutableLiveData()
        private var hasNavigationButton = false
        private var navigationButtonClickListener: ((view: View) -> Unit)? = null
        private var badgeCount: MutableLiveData<Int> = MutableLiveData()
        private var scrollBehavior: ToolbarScrollBehavior = ToolbarScrollBehavior.SCROLL_OUT
        private val menuResId: MutableLiveData<Int> = MutableLiveData()
        private var menuItemClickListener: ((menu: MenuItem) -> Boolean)? = null
        private var show: Boolean = true

        /**
         * View-ID of the imported layout (needs to be [com.lidl.eci.R.layout.view_toolbar]
         * @param resId ID of the layout
         */
        fun withId(@IdRes resId: Int) = apply { this.resId = resId }

        /**
         * Title to display on the Toolbar. The [ToolbarModel.title] property is a [MutableLiveData] Object, so the Toolbar Title can also be
         * re-adjusted.
         *
         * @param title Title to display
         */
        fun withTitle(title: String) = apply { this.title.value = title }

        /**
         * Menu to inflate on the Toolbar. The [ToolbarModel.menuResId] property is a [MutableLiveData] Object, so the Toolbar Menu can also be
         * re-adjusted.
         *
         * @param menuId ID to inflate. Leave this field empty if no menu should be shown. To get the MenuItems translated automatically by the
         * TranslationUtils, use the translation-key as the text-value in the menu.xml
         * eg.
         * <item
         *      android:id="@+id/action_reset_filter"
         *      android:title="reset_filter"
         *      app:showAsAction="always|withText"/>
         *
         * @param menuItemClickListener function which is triggered as soon as a [MenuItem] is clicked.
         */
        fun withMenu(@MenuRes menuId: Int = 0, menuItemClickListener: ((menuItem: MenuItem) -> Boolean)?) = apply {
            this.menuResId.value = menuId
            this.menuItemClickListener = menuItemClickListener
        }

        /**
         * Display the Navigation-Back-Button. When the Back-Button is clicked, [FragmentActivity.onBackPressed] is called.
         */
        fun withDefaultNavigationButton() = apply {
            this.hasNavigationButton = true
        }

        /**
         * Displays the Navigation-Back-Button with a Custom Click-Function
         *
         * @param clickListener function which is triggered as soon as the Navigation-Button is clicked.
         */
        fun withNavigationButton(clickListener: ((view: View) -> Unit)? = null) = apply {
            this.hasNavigationButton = true
            this.navigationButtonClickListener = clickListener
        }

        /**
         * Displays a Badge with the given [badgeCount] to the right of the title. The [ToolbarModel.badgeCount] property is a [MutableLiveData] Object,
         * so the Toolbar Badge can also be re-adjusted.
         *
         * @param badgeCount count to display on the Badge
         */
        fun withBadge(badgeCount: Int) = apply {
            this.badgeCount.value = badgeCount
        }

        /**
         * There are two Toolbar-Behaviors in the App when User is scrolling the display content.
         * [ToolbarScrollBehavior.FIXED] The Toolbar stays at the top when the content moves
         * [ToolbarScrollBehavior.SCROLL_OUT] When scrolling down, the Toolbar is moved out to create more space on the display. When scrolling upwards,
         * the Toolbar is retracted again. To use this Scrolling-Behavior the Toolbar needs to have a CoordinatorLayout as it´s parent Layout.
         *
         * @param scrollBehavior scrolling behavior of the Toolbar
         */
        fun withScrollBehavior(scrollBehavior: ToolbarScrollBehavior) = apply {
            this.scrollBehavior = scrollBehavior
        }

        /**
         * Allows you to show and hide the Toolbar.
         *
         * @param show visibility of the Toolbar
         */
        fun withVisibility(show: Boolean) = apply {
            this.show = show
        }

        /**
         * Builds the [ToolbarModel]
         */
        fun build() = ToolbarModel(
            resId,
            title,
            show,
            hasNavigationButton,
            navigationButtonClickListener,
            scrollBehavior,
            menuResId,
            menuItemClickListener
        )

    }
}
