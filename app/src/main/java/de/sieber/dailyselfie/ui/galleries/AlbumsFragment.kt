package de.sieber.dailyselfie.ui.galleries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentAlbumsBinding
import kotlinx.android.synthetic.main.fragment_albums.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class AlbumsFragment : Fragment() {

    private val vmAlbums by viewModel<AlbumViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentAlbumsBinding>(
                inflater,
                R.layout.fragment_albums,
                container,
                false
            )
        ) {
            vmGalleries = this@AlbumsFragment.vmAlbums
            lifecycleOwner = this@AlbumsFragment
            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val galleriesAdapter = AlbumAdapter { gallery ->
            // todo handle gallery click
        }
        with(rvAlbums) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = galleriesAdapter
        }

        vmAlbums.albums.observe(this, Observer { galleries ->
            galleries?.let {
                galleriesAdapter.setGalleries(galleries)
            }
        })
    }
    //endregion Fragment Lifecycle
}