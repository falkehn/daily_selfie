package de.sieber.dailyselfie.ui.common

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import androidx.exifinterface.media.ExifInterface
import java.io.File

class FileUtils {

    fun getBitmapFromFilePath(filePath: String): Bitmap? {
        try {
            val image = File(filePath)
            val bmOptions = BitmapFactory.Options()
            bmOptions.inPreferredConfig = Bitmap.Config.ARGB_8888
            val sourceBitmap = BitmapFactory.decodeFile(image.absolutePath, bmOptions)
            val exif = ExifInterface(filePath)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
            val matrix = Matrix()
            when (orientation) {
                6 -> matrix.postRotate(90f)
                3 -> matrix.postRotate(180f)
                8 -> matrix.postRotate(270f)
            }
            return Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.width, sourceBitmap.height, matrix, true)
        } catch (exception: Exception) {
            return null
        }
    }

}