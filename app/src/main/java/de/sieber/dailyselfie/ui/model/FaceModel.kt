package de.sieber.dailyselfie.ui.model

import android.graphics.Rect

data class FaceModel(
    val headEulerAngleY: Float = 0f,
    val boundaries: Rect = Rect()
)