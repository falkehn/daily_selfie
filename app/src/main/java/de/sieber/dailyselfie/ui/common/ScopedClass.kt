package de.sieber.dailyselfie.ui.common

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class ScopedClass(
    private val dispatcher: CoroutineDispatcher
) : CoroutineScope {

    // This job can be used to cancel any threads still running upon destruction of the scopedclass
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + dispatcher
}