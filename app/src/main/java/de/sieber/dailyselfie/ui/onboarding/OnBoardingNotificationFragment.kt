package de.sieber.dailyselfie.ui.onboarding

import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentOnboardingNotificationBinding
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import kotlinx.android.synthetic.main.fragment_onboarding_notification.btnSetTime
import kotlinx.android.synthetic.main.fragment_onboarding_notification.swNotifications
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class OnBoardingNotificationFragment : BaseOnBoardingFragment() {

    private val preferenceHelper by inject<PreferencesHelper>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentOnboardingNotificationBinding>(
                inflater,
                R.layout.fragment_onboarding_notification,
                container,
                false
            )
        ) {
            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            swNotifications.isChecked = preferenceHelper.notificationsEnabled
            swNotifications.onCheckedChange { _, isChecked ->
                preferenceHelper.notificationsEnabled = isChecked
                btnSetTime.isEnabled = isChecked
            }

            btnSetTime.text = getString(R.string.reminder_at, preferenceHelper.notificationsTime)
            btnSetTime.onClick {
                showTimePicker(context)
            }
        }
    }

    override fun playAnimation() = Unit

    private fun showTimePicker(context: Context) {
        val calendar = Calendar.getInstance()
        TimePickerDialog(
            context,
            R.style.date_and_time_picker,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                calendar.set(Calendar.MINUTE, minute)
                val time = simpleDateFormat.format(calendar.time)
                preferenceHelper.notificationsTime = time
                btnSetTime.text = getString(R.string.reminder_at, time)

            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            true
        ).show()
    }

}