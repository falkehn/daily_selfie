package de.sieber.dailyselfie.ui.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentOnboardingCreateAlbumBinding
import de.sieber.dailyselfie.ui.extensions.onTextChanged
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class OnBoardingCreateAlbumFragment : BaseOnBoardingFragment() {

    private val vmOnBoarding by sharedViewModel<OnBoardingViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentOnboardingCreateAlbumBinding>(
                inflater,
                R.layout.fragment_onboarding_create_album,
                container,
                false
            )
        ) {
            etAlbumName.onTextChanged { albumName ->
                vmOnBoarding.albumName.postValue(albumName.trim())
            }
            return root
        }
    }

    override fun playAnimation() = Unit

}