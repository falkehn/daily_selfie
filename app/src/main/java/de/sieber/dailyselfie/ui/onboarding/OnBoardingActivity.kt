package de.sieber.dailyselfie.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.Button
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.badoualy.stepperindicator.StepperIndicator
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.ActivityOnboardingBinding
import de.sieber.dailyselfie.ui.extensions.coroutineScope
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_onboarding.btnNext
import kotlinx.coroutines.launch
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.standalone.KoinComponent


class OnBoardingActivity : FragmentActivity(), KoinComponent {

    private val vmOnBoarding by viewModel<OnBoardingViewModel>()
    private val vmGooglePhotos by viewModel<GooglePhotosViewModel>()

    private var onBoardingViewPagerAdapter = OnBoardingPagerAdapter(supportFragmentManager, useLoginScreen = true)
    private val vpOnBoarding by lazy {
        findViewById<ViewPager>(R.id.vpOnboarding)
    }
    private val indicator by lazy {
        findViewById<StepperIndicator>(R.id.vIndicator)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val dataBinding = DataBindingUtil.setContentView<ActivityOnboardingBinding>(this, R.layout.activity_onboarding)
        dataBinding.viewModel = vmOnBoarding
        dataBinding.lifecycleOwner = this
        dataBinding.executePendingBindings()

        vpOnBoarding.adapter = onBoardingViewPagerAdapter
        vpOnBoarding.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) = Unit

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) = Unit

            override fun onPageSelected(position: Int) {
                (onBoardingViewPagerAdapter.getItem(position) as BaseOnBoardingFragment).playAnimation()
                updateButtonText(vpOnBoarding)
            }

        })

        indicator.setViewPager(vpOnBoarding, false)

        val buttonNext = findViewById<Button>(R.id.btnNext)
        buttonNext.onClick {
            showNextOnBoardingScreen()
        }

        vmOnBoarding.logoutUser()

        addViewModelObserver()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = onBoardingViewPagerAdapter.getItem(vpOnBoarding.currentItem)
        fragment.onActivityResult(requestCode, resultCode, data)
    }

    private fun removeLoginScreen() {
        onBoardingViewPagerAdapter = OnBoardingPagerAdapter(supportFragmentManager, useLoginScreen = false)
        indicator.stepCount = 3
        indicator.currentStep = 0
        vpOnBoarding.adapter = onBoardingViewPagerAdapter
        vpOnBoarding.currentItem = 1
    }

    private fun showNextOnBoardingScreen() {
        val isLastPosition = vpOnBoarding.currentItem == onBoardingViewPagerAdapter.count - 1
        if (isLastPosition) {
            finishOnBoardingProcess()
        } else {
            vpOnBoarding.currentItem = vpOnBoarding.currentItem + 1
        }
        updateButtonText(vpOnBoarding)
    }

    private fun finishOnBoardingProcess() {
        vmOnBoarding.finishOnBoardingProcess()
    }

    private fun updateButtonText(vpOnBoarding: ViewPager) {
        val isLastPosition = vpOnBoarding.currentItem == onBoardingViewPagerAdapter.count - 1
        btnNext.text = if (isLastPosition) getString(R.string.lets_go) else getString(R.string.next)
    }

    private fun addViewModelObserver() {
        with(vmOnBoarding) {
            showNextScreen.observe(this@OnBoardingActivity, Observer {
                it.getContentIfNotHandled()?.let {
                    showNextOnBoardingScreen()
                }
            })
            finishOnBoarding.observe(this@OnBoardingActivity, Observer {
                it.getContentIfNotHandled()?.let {
                    finishOnBoardingProcess()
                }
            })
            getDestroyActivity().observe(this@OnBoardingActivity, Observer {
                val intent = Intent(this@OnBoardingActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            })
            isFinishingOnBoardingProcess().observe(this@OnBoardingActivity, Observer { isFinishingOnBoardingProcess ->
                if (isFinishingOnBoardingProcess) {
                    window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                } else {
                    window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                }
            })
        }

        vmGooglePhotos.userSignedIn.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                coroutineScope.launch {
                    if (vmOnBoarding.isANewUser()) {
                        removeLoginScreen()
                    } else {
                        finishOnBoardingProcess()
                    }
                }
            }
        })
    }

}
