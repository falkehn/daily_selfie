package de.sieber.dailyselfie.ui.selfies

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.databinding.ItemSelfieHeaderBinding
import de.sieber.dailyselfie.databinding.ItemSelfiePhotoBinding
import de.sieber.dailyselfie.ui.model.SelfieHeaderItemModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel

sealed class SelfieViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    class SelfieItemViewHolder(
        private val binding: ItemSelfiePhotoBinding
    ) : SelfieViewHolder(binding.root) {

        fun bind(
            selfieItemModel: SelfiePhotoItemModel,
            onSelfieClick: (imageView: ImageView, positionInList: Int) -> Unit,
            onSelfieLongClick: (positionInList: Int) -> Unit
        ) {
            with(binding) {
                this.selfieItemModel = selfieItemModel
                this.executePendingBindings()
                itemView.setOnLongClickListener {
                    onSelfieLongClick(adapterPosition)
                    true
                }
                itemView.setOnClickListener {
                    onSelfieClick(ivPhoto, adapterPosition)
                }
            }
        }
    }

    class SelfiesHeaderViewHolder(
        private val binding: ItemSelfieHeaderBinding
    ) : SelfieViewHolder(binding.root) {

        fun bind(
            selfieHeaderItemModel: SelfieHeaderItemModel
        ) {
            with(binding) {
                this.title = selfieHeaderItemModel.title
                this.description = selfieHeaderItemModel.description
            }
        }
    }

}