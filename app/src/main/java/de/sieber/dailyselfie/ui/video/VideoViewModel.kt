package de.sieber.dailyselfie.ui.video

import android.app.Application
import android.net.Uri
import androidx.core.content.FileProvider
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.extensions.getVideoFilePath
import kotlinx.coroutines.Dispatchers
import java.io.File

class VideoViewModel(
    private val application: Application,
    private val albumRepository: AlbumRepository
) : ScopedViewModel(Dispatchers.IO) {

    fun getVideoUri(albumId: Long): Uri = FileProvider.getUriForFile(
        application,
        "de.sieber.dailyselfie.provider",
        File(
            getVideoFilePath(
                application, albumId
            )
        )
    )

    fun getAlbumId(): Long {
        val preferencesHelper = PreferencesHelper(application)
        return preferencesHelper.lastSelectedSelfieAlbum
    }

    fun getAlbumName(): String {
        val albumId = getAlbumId()
        return albumRepository.getAlbum(albumId).name
    }

}