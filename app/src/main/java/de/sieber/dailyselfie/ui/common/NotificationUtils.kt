package de.sieber.dailyselfie.ui.common

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import de.sieber.dailyselfie.ui.common.notification.BootReceiver
import de.sieber.dailyselfie.ui.common.notification.SelfieReminderBroadcastReceiver
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

const val SELFIE_REMINDER_ID = 28122015

class NotificationUtils(
    private val context: Context
) {

    /**
     * @param notificationTime Formatted in HH:mm e.g. 08:00
     */
    fun createRepeatingAlarmManager(notificationTime: String) {
        cancelAlarmManager()
        val pendingIntent = getNotificationIntent()
        val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.GERMANY)
        val date = simpleDateFormat.parse(notificationTime)

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, date.hours)
        calendar.set(Calendar.MINUTE, date.minutes)
        calendar.set(Calendar.SECOND, 0)
        var startUpTime = calendar.timeInMillis
        if (System.currentTimeMillis() > startUpTime) {
            startUpTime += 24 * 60 * 60 * 1000
        }

        val receiver = ComponentName(context, BootReceiver::class.java)
        context.packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, startUpTime, AlarmManager.INTERVAL_FIFTEEN_MINUTES, pendingIntent)
    }

    fun cancelAlarmManager() {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = getNotificationIntent()
        alarmManager.cancel(pendingIntent)
    }

    private fun getNotificationIntent(): PendingIntent {
        val reminderService = Intent(context, SelfieReminderBroadcastReceiver::class.java)
        return PendingIntent.getBroadcast(
            context,
            SELFIE_REMINDER_ID,
            reminderService,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

}