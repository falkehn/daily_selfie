package de.sieber.dailyselfie.ui.google.viewmodel

import android.app.Application
import android.content.Intent
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.PagedList
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.photos.library.v1.PhotosLibraryClient
import com.google.photos.library.v1.PhotosLibrarySettings
import com.google.photos.library.v1.proto.BatchCreateMediaItemsRequest
import com.google.photos.library.v1.upload.UploadMediaItemRequest
import com.google.photos.types.proto.MediaItem
import de.sieber.dailyselfie.database.dao.SelfieDao
import de.sieber.dailyselfie.database.entities.OAuthCredentialsEntity
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.database.repository.OAuthCredentialsRepository
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.extensions.getTimeString
import de.sieber.dailyselfie.ui.google.auth.PhotosOAuth2Credentials
import de.sieber.dailyselfie.ui.google.paging.GooglePhotosRepository
import de.sieber.dailyselfie.ui.model.GooglePhotoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import timber.log.Timber
import java.io.IOException
import java.io.RandomAccessFile
import java.util.Calendar


const val WEB_CLIENT_ID = "325999980938-pi7mr75sfieo4e3gkca714779qo3pnod.apps.googleusercontent.com"
const val WEB_CLIENT_SECRET = "wf2pUePLIHwTH3o3UiG2CvBv"

class GooglePhotosViewModel(
    val application: Application,
    val oAuthCredentialsRepository: OAuthCredentialsRepository,
    val selfieDao: SelfieDao,
    val albumRepository: AlbumRepository
) : ScopedViewModel(Dispatchers.IO) {

    private val googlePhotosLibraryClient by lazy {
        val credentials = PhotosOAuth2Credentials(oAuthCredentialsRepository)

        val settings = PhotosLibrarySettings.newBuilder()
            .setCredentialsProvider(
                FixedCredentialsProvider.create(
                    credentials
                )
            )
            .build()

        PhotosLibraryClient.initialize(settings)
    }
    private var googleApiClient: GoogleApiClient? = null
    val userSignedIn = MutableLiveData<Event<Boolean>>()

    private var googlePhotosRepository: GooglePhotosRepository? = null

    val googlePhotosApiIsReadyToUse = MutableLiveData<Event<Boolean>>()

    private val photosListing = Transformations.map(googlePhotosApiIsReadyToUse) {
        getGooglePhotosRepository().getGooglePhotos()
    }

    val photosModels: LiveData<PagedList<GooglePhotoModel>> =
        Transformations.switchMap(photosListing) {
            it?.pagedList
        }
    val photosIsLoading: LiveData<Boolean> = Transformations.switchMap(photosListing) {
        it?.isInitialLoading
    }

    val isFinishedImporting = MutableLiveData<Event<Boolean>>()
    val isImporting = MutableLiveData<Boolean>()

    private val isLoadingAlbums = MutableLiveData<Boolean>()

    //region Login/ Authentication -------------------------------------------------------------------------------------
    fun createGoogleApiClient(activity: FragmentActivity) {
        if (googleApiClient == null) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(WEB_CLIENT_ID)
                .requestEmail()
                .requestServerAuthCode(WEB_CLIENT_ID)
                .requestScopes(
                    Scope("https://www.googleapis.com/auth/photoslibrary")
                )
                .build()

            googleApiClient = GoogleApiClient.Builder(application)
                .enableAutoManage(activity, null)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
        }
    }

    private fun getSignedInUser() = GoogleSignIn.getLastSignedInAccount(application)

    fun isUserSignedIn() = getSignedInUser() != null

    fun hasValidAuthToken(): Boolean {
        val expiration = oAuthCredentialsRepository.getOAuthCredentials()?.expiresIn ?: 0
        val now = Calendar.getInstance().timeInMillis
        return getSignedInUser()?.isExpired == false || expiration > now
    }

    fun alreadyLoggedIn() = oAuthCredentialsRepository.getOAuthCredentials() != null

    fun getSignInIntent(): Intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient)

    fun handleSignInResult(data: Intent, onSuccess: () -> Unit, onFailure: () -> Unit) {
        launch(Dispatchers.IO) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                // [START get_auth_code]
                val acct = result.signInAccount
                acct?.let {
                    val authCode = acct.serverAuthCode
                    authCode?.let {
                        requestOAuthCredentials(authCode) { oAuthCredentialsEntity ->
                            oAuthCredentialsRepository.insertOAuthCredentials(oAuthCredentialsEntity)
                            onSuccess()
                        }
                    }
                }
            } else {
                onFailure()
            }
        }
    }

    private fun requestOAuthCredentials(
        authCode: String,
        onSuccess: (oAuthCredentials: OAuthCredentialsEntity) -> Unit
    ) {
        val client = OkHttpClient()

        val requestBody = FormBody.Builder()
            .add("grant_type", "authorization_code")
            .add("client_id", WEB_CLIENT_ID)
            .add("client_secret", WEB_CLIENT_SECRET)
            .add("redirect_uri", "")
            .add("code", authCode)
            .build()
        val request = Request.Builder()
            .url("https://www.googleapis.com/oauth2/v4/token")
            .post(requestBody)
            .build()
        client.newCall(request).enqueue(object : Callback {

            override fun onResponse(call: Call, response: Response) {
                val jsonObject = JSONObject(response.body()?.string())
                val accessToken = jsonObject.getString("access_token")
                val expiresIn = jsonObject.getLong("expires_in")
                val expiresDate = Calendar.getInstance().timeInMillis + expiresIn
                val idToken = jsonObject.getString("id_token")

                onSuccess(
                    OAuthCredentialsEntity(
                        accessToken = accessToken,
                        expiresIn = expiresDate,
                        idToken = idToken
                    )
                )

            }

            override fun onFailure(call: Call, e: IOException) = e.printStackTrace()

        })
    }

    fun initializePhotoLibraryClient() {
        googlePhotosRepository =
            GooglePhotosRepository(
                application,
                googlePhotosLibraryClient,
                selfieDao,
                albumRepository
            )
        googlePhotosApiIsReadyToUse.postValue(Event(true))
    }

    fun logout() {
        googleApiClient?.disconnect()
        googleApiClient?.maybeSignOut()
        oAuthCredentialsRepository.deleteAll()
    }
    //endregion Login/ Authentication

    //region Photos ----------------------------------------------------------------------------------------------------
    fun downloadSelectedPhotos() {
        val localAlbumId = getCurrentActiveAlbumId()
        val googlePhotosRepository = getGooglePhotosRepository()
        val googlePhotos = photosModels.value ?: return

        googlePhotosRepository.downloadPhoto(
            application,
            googlePhotos.filter { it.isSelected.get() },
            isImporting,
            isFinishedImporting,
            localAlbumId
        )
    }

    fun removeSelectedPhotosFromAlbum(googlePhotosId: String) = launch {
        val localAlbumId = getCurrentActiveAlbumId()
        val albumEntity = albumRepository.getAlbum(localAlbumId)
        getGooglePhotosRepository().removePhotoFromGooglePhotosAlbum(albumEntity.googlePhotosAlbumId, googlePhotosId)
    }
    //endregion Photos

    //region Albums ----------------------------------------------------------------------------------------------------
    fun createAlbum(albumTitle: String): String {
        return if (isUserSignedIn()) {
            getGooglePhotosRepository().createAlbum(albumTitle)
        } else {
            ""
        }
    }

    fun setGooglePhotosAlbumId(albumId: String) {
        val albumEntity = albumRepository.getAlbum(getCurrentActiveAlbumId())
        albumEntity.googlePhotosAlbumId = albumId
        albumRepository.updateAlbum(albumEntity)
    }

    private fun getCurrentActiveAlbumId(): Long {
        val preferencesHelper = PreferencesHelper(application)
        return preferencesHelper.lastSelectedSelfieAlbum
    }

    fun isLoadingAlbums(): LiveData<Boolean> = isLoadingAlbums
    //endregion Albums

    fun getPhotosOfAlbum(googlePhotosAlbumId: String): List<MediaItem>? {
        val repository = getGooglePhotosRepository()
        return repository.listPhotosOfAlbum(googlePhotosAlbumId)
    }

    fun googlePhotosAlbumExists(googlePhotosAlbumId: String): Boolean {
        val repository = getGooglePhotosRepository()
        return repository.existsAlbum(googlePhotosAlbumId)
    }

    fun createGooglePhotosAlbum(googlePhotosAlbumName: String): String? {
        val repository = getGooglePhotosRepository()
        return repository.createAlbum(googlePhotosAlbumName)
    }

    /**
     * The upload process
     *
     * Uploading media items is a two-step process:
     *
     * 1. Upload the raw bytes to a Google Server. This doesn't result in any media items being created in the user’s
     * Google Photos account. Instead, it returns an upload token which identifies the uploaded bytes.
     *
     * 2. Use the upload token to create the media item in the user’s Google Photos account. You can choose whether the
     * media should be also added to a specific album. For more information, see Create albums.
     */
    fun uploadSelfie(selfieEntity: SelfieEntity) {
        val pathToFile = selfieEntity.filePath
        val mediaFileName = pathToFile.substring(pathToFile.lastIndexOf("/") + 1)
        // Specify the filename that will be shown to the user in Google Photos
        // and the path to the file that will be uploaded
        val uploadRequest = UploadMediaItemRequest.newBuilder()
            //filename of the media item along with the file extension
            .setFileName(mediaFileName)
            .setDataFile(RandomAccessFile(pathToFile, "r"))
            .build()

        // Upload and capture the response
        val uploadResponse = googlePhotosLibraryClient.uploadMediaItem(uploadRequest)
        uploadResponse?.let {
            if (uploadResponse.error.isPresent) {
                Timber.e("Error occurred: ${uploadResponse.error.get().cause}")
            } else {
                // If the upload is successful, get the uploadToken
                val uploadToken = uploadResponse.uploadToken
                // Use this upload token to create a media item
                Timber.i("Uploaded MediaItem: $uploadToken")

                val albumEntity = albumRepository.getAlbum(selfieEntity.albumId)
                val createRequestBuilder = BatchCreateMediaItemsRequest.newBuilder()
                createRequestBuilder.albumId = albumEntity.googlePhotosAlbumId
                createRequestBuilder
                    .addNewMediaItemsBuilder()
                    .simpleMediaItemBuilder.uploadToken = uploadResponse.uploadToken.get()

                val response = googlePhotosLibraryClient.batchCreateMediaItems(createRequestBuilder.build())
                val googlePhotoId = response.newMediaItemResultsList.first().mediaItem.id
                selfieEntity.googlePhotosId = googlePhotoId
                selfieDao.updateSelfieEntity(selfieEntity)
                Timber.i("Added MediaItem successfully to Google Photos Album. ID: $googlePhotoId")
            }
        }
    }

    /**
     * This function iterates over all Selfies and checks if there are some Selfies which need´s to be uploaded.
     */
    fun syncPhotos() {
        launch {
            Timber.i("Start sync Photos with Google Account")
            val albumId = getCurrentActiveAlbumId()
            albumRepository.getAlbumWithSelfies(albumId)?.let { albumWithSelfies ->
                Timber.i("Sync Photos of Album: ${albumWithSelfies.albumEntity?.name} - ID: ${albumWithSelfies.albumEntity?.googlePhotosAlbumId}")
                albumWithSelfies.selfies?.let { selfies ->
                    try {
                        // check if user may put some photos via the google photos app into the album which needs to be downloaded
                        val photosOfAlbum = getGooglePhotosRepository().listPhotosOfAlbum(albumWithSelfies.albumEntity?.googlePhotosAlbumId ?: "")
                        val selfieGooglePhotosId = selfies.map { it.googlePhotosId }

                        // check which photos needs to be downloaded
                        val photosToDownload = photosOfAlbum
                            .filter {
                                it.id !in selfieGooglePhotosId
                            }.map {
                                createGooglePhoto(it)
                            }
                        Timber.i("Need to Download: $photosOfAlbum")
                        getGooglePhotosRepository().downloadPhoto(
                            application,
                            photosToDownload,
                            isImporting,
                            isFinishedImporting,
                            albumId
                        )

                        // check which photos needs to be deleted
                        val photosOfAlbumIds = photosOfAlbum.map { it.id }
                        Timber.i("Photos of Album (${photosOfAlbumIds.size}): $photosOfAlbumIds")
                        val photosToDelete = selfies
                            .filter { selfie ->
                                selfie.googlePhotosId.isNotEmpty() && selfie.googlePhotosId !in photosOfAlbumIds
                            }
                        photosToDelete.forEach {
                            selfieDao.deleteSelfieEntity(it.id)
                        }
                        Timber.i("Need to Delete (${photosToDelete.size}): $photosToDelete")

                        // check which photos needs to be uploaded
                        selfies
                            .filter {
                                it.googlePhotosId.isEmpty()
                            }
                            .forEach { selfie ->
                                Timber.i("Upload: $selfie")
                                uploadSelfie(selfie)
                            }
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }
                }
            }
        }
    }

    private fun createGooglePhoto(mediaItem: MediaItem) = GooglePhotoModel(
        fileName = mediaItem.filename,
        smallImageUrl = mediaItem.baseUrl,
        largeImageUrl = "${mediaItem.baseUrl}=w2048-h1024",
        creationTimeAsString = mediaItem.mediaMetadata.creationTime.seconds.getTimeString(isInSeconds = true),
        creationTimeInSeconds = mediaItem.mediaMetadata.creationTime.seconds,
        id = mediaItem.id
    )

    private fun getGooglePhotosRepository(): GooglePhotosRepository =
        googlePhotosRepository ?: throw IllegalStateException("GooglePhotosRepository is not initialized.")

}