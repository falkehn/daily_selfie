package de.sieber.dailyselfie.ui.onboarding

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.firebase.ui.auth.AuthUI
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.database.entities.AlbumEntity
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.common.FirebaseUtils
import de.sieber.dailyselfie.ui.common.GooglePhotosAlbum
import de.sieber.dailyselfie.ui.common.NotificationSettings
import de.sieber.dailyselfie.ui.common.NotificationUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import de.sieber.dailyselfie.ui.common.ScopedViewModel
import de.sieber.dailyselfie.ui.extensions.getTimeString
import de.sieber.dailyselfie.ui.google.paging.GooglePhotosRepository
import de.sieber.dailyselfie.ui.model.GooglePhotoModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.Arrays

class OnBoardingViewModel(
    val application: Application,
    private val albumRepository: AlbumRepository,
    private val googlePhotosRepository: GooglePhotosRepository,
    private val notificationUtils: NotificationUtils
) : ScopedViewModel(Dispatchers.IO) {

    private val firebaseUtils = FirebaseUtils()
    private val preferencesHelper = PreferencesHelper(application)

    val showNextScreen = MutableLiveData<Event<Boolean>>()
    val finishOnBoarding = MutableLiveData<Event<Boolean>>()

    private val isFinishingOnBoardingProcess = MutableLiveData<Boolean>()
    private val downloadDescription = MutableLiveData<String>()
    private val destroyActivity = MutableLiveData<Boolean>()

    val albumName = MutableLiveData<String>()

    fun getFirebaseAuthenticationProviders(): MutableList<AuthUI.IdpConfig> = Arrays.asList(
        AuthUI.IdpConfig.GoogleBuilder().build()
    )

    fun finishOnBoardingProcess() {
        // check if user has used login
        isFinishingOnBoardingProcess.postValue(true)
        launch {
            if (firebaseUtils.userLoggedIn()) {
                val firebaseUser = firebaseUtils.getFirebaseUser()
                val isANewUser = firebaseUser == null
                if (isANewUser) {
                    // setup notification
                    val notificationTime = preferencesHelper.notificationsTime
                    createNotification(notificationTime)

                    // add album to google photos
                    val albumTitle = getAlbumTitle()
                    val googlePhotosAlbumId = googlePhotosRepository.createAlbum(albumTitle)
                    addDefaultAlbumToDatabase(googlePhotosAlbumId)
                    selectDefaultAlbum()

                    firebaseUtils.addNewUserToFirebase(
                        notificationSettings = NotificationSettings(
                            enabled = preferencesHelper.notificationsEnabled,
                            time = preferencesHelper.notificationsTime
                        ),
                        googlePhotosAlbum = GooglePhotosAlbum(
                            name = albumTitle,
                            id = googlePhotosAlbumId
                        )
                    )
                } else {
                    // get settings from old application
                    firebaseUser?.let { it ->
                        albumRepository.deleteAll()

                        preferencesHelper.notificationsEnabled = it.notificationSettings.enabled
                        preferencesHelper.notificationsTime = it.notificationSettings.time
                        createNotification(it.notificationSettings.time)

                        val allPhotoToDownload = mutableListOf<GooglePhotoModel>()
                        it.googlePhotosAlbum.forEach { album ->
                            val localeAlbumId = addAlbumToDatabase(album.name, album.id)
                            val photosOfAlbum = getPhotosToDownloadOfAlbum(album.id, localeAlbumId)
                            allPhotoToDownload.addAll(photosOfAlbum)
                        }

                        val photosToDownload = allPhotoToDownload.distinctBy { googlePhotoModel ->
                            googlePhotoModel.fileName
                        }

                        downloadDescription.postValue(
                            application.getString(
                                R.string.images_downloaded,
                                "0", photosToDownload.size.toString()
                            )
                        )
                        googlePhotosRepository.downloadPhoto(
                            application,
                            photosToDownload
                        ) { downloaded, totalDownload ->
                            downloadDescription.postValue(
                                application.getString(
                                    R.string.images_downloaded,
                                    downloaded.toString(), totalDownload.toString()
                                )
                            )
                        }
                        selectDefaultAlbum()
                    }
                }
            } else {
                // user which is only want to locally use this app
                addDefaultAlbumToDatabase()
                selectDefaultAlbum()
                // setup notification
                val notificationTime = preferencesHelper.notificationsTime
                createNotification(notificationTime)
            }

            isFinishingOnBoardingProcess.postValue(false)
            preferencesHelper.onBoardingFinished = true
            destroyActivity.postValue(true)
        }
    }

    private fun addAlbumToDatabase(albumTitle: String, googlePhotosAlbumId: String = ""): Long {
        // insert gallery with name
        val albumEntity = AlbumEntity(name = albumTitle, googlePhotosAlbumId = googlePhotosAlbumId)
        return albumRepository.addAlbum(albumEntity)
    }

    private fun addDefaultAlbumToDatabase(googlePhotosAlbumId: String = "") {
        // insert gallery with name
        val albumName = getAlbumTitle()
        val albumEntity = AlbumEntity(name = albumName, googlePhotosAlbumId = googlePhotosAlbumId)
        albumRepository.addAlbum(albumEntity)
    }

    private fun selectDefaultAlbum() {
        // select the album as the last selected
        val albumId = albumRepository.getAlbumsWithSelfies().first().albumEntity?.id ?: 0
        preferencesHelper.lastSelectedSelfieAlbum = albumId
    }

    private fun getAlbumTitle() = albumName.value ?: application.getString(R.string.album_default_name)

    fun getDestroyActivity(): LiveData<Boolean> = destroyActivity

    fun isFinishingOnBoardingProcess(): LiveData<Boolean> = isFinishingOnBoardingProcess

    fun getDownloadDescription(): LiveData<String> = downloadDescription

    private fun getPhotosToDownloadOfAlbum(googlePhotosAlbumId: String, localAlbumId: Long): List<GooglePhotoModel> {
        val photosOfAlbum = googlePhotosRepository.listPhotosOfAlbum(googlePhotosAlbumId)
        return photosOfAlbum
            .map { mediaItem ->
                GooglePhotoModel(
                    fileName = mediaItem.filename,
                    smallImageUrl = mediaItem.baseUrl,
                    largeImageUrl = "${mediaItem.baseUrl}=w2048-h1024",
                    creationTimeAsString = mediaItem.mediaMetadata.creationTime.seconds.getTimeString(isInSeconds = true),
                    creationTimeInSeconds = mediaItem.mediaMetadata.creationTime.seconds,
                    id = mediaItem.id,
                    localAlbumId = localAlbumId
                )
            }
    }

    private fun createNotification(time: String) = notificationUtils.createRepeatingAlarmManager(time)

    suspend fun isANewUser(): Boolean {
        return if (firebaseUtils.userLoggedIn()) {
            val firebaseUser = firebaseUtils.getFirebaseUser()
            firebaseUser == null
        } else {
            false
        }
    }

    fun logoutUser() = firebaseUtils.logout()

}