package de.sieber.dailyselfie.ui.common.fragment

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import de.sieber.dailyselfie.R

open class SimpleBaseDialogFragment : DialogFragment() {

    //region Lifecycle -------------------------------------------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFragmentStyle)
    }
    //endregion Lifecycle

}
