package de.sieber.dailyselfie.ui.google.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentGooglePhotosContainerBinding
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseDialogFragment
import de.sieber.dailyselfie.ui.google.adapter.GooglePhotosContainerTabsAdapter
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class GooglePhotosContainerFragment : SimpleBaseDialogFragment() {

    private val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentGooglePhotosContainerBinding>(
                inflater,
                R.layout.fragment_google_photos_container,
                container,
                false
            )
        ) {
            vmGooglePhotos = this@GooglePhotosContainerFragment.vmGooglePhotos
            executePendingBindings()
            lifecycleOwner = this@GooglePhotosContainerFragment

            val tabsAdapter = GooglePhotosContainerTabsAdapter(childFragmentManager)
            tabsAdapter.addFragment(getString(R.string.tab_all_photos), GooglePhotosAllPhotosFragment())
//            tabsAdapter.addFragment(getString(R.string.tab_albums), GooglePhotosAlbumsFragment())

            tabsViewPager.adapter = tabsAdapter
            tabLayout.setupWithViewPager(tabsViewPager)

            ivClose.onClick {
                dismiss()
            }

            addViewModelObserver()

            return root
        }
    }
    //endregion Fragment Lifecycle

    private fun addViewModelObserver() {
        vmGooglePhotos.isFinishedImporting.observe(viewLifecycleOwner, Observer { finished ->
            finished.peekContent().let {
                if (it) {
                    dismiss()
                }
            }
        })
    }

}