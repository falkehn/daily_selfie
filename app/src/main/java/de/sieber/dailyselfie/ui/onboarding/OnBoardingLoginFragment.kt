package de.sieber.dailyselfie.ui.onboarding

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.firebase.ui.auth.AuthUI
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentOnboardingLoginBinding
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.selfies.RC_GOOGLE_SIGN_IN
import kotlinx.android.synthetic.main.fragment_onboarding_login.btnLogin
import kotlinx.android.synthetic.main.fragment_onboarding_login.btnSkip
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


const val RC_SIGN_IN = 1

/**
 * https://firebase.google.com/docs/auth/android/firebaseui
 */
class OnBoardingLoginFragment : BaseOnBoardingFragment() {

    private val vmGooglePhotos by sharedViewModel<GooglePhotosViewModel>()
    private val vmOnBoarding by sharedViewModel<OnBoardingViewModel>()

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.let {
            vmGooglePhotos.createGoogleApiClient(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentOnboardingLoginBinding>(
                inflater,
                R.layout.fragment_onboarding_login,
                container,
                false
            )
        ) {
            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnLogin.onClick {
            showFireBaseLogin()
        }
        btnSkip.onClick {
            vmOnBoarding.showNextScreen.postValue(Event(true))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == Activity.RESULT_OK) {
                // Successfully signed in
                signInForGooglePhotos()
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        } else if (requestCode == RC_GOOGLE_SIGN_IN) {
            // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            data?.let {
                vmGooglePhotos.handleSignInResult(
                    data = it,
                    onSuccess = {
                        vmGooglePhotos.userSignedIn.postValue(Event(true))
                        vmOnBoarding.showNextScreen.postValue(Event(true))
                    },
                    onFailure = {
                        vmGooglePhotos.userSignedIn.postValue(Event(false))
                    }
                )
            }
        }

    }
    //endregion Fragment-Lifecycle

    override fun playAnimation() {
        // No Animation on this Screen
    }

    private fun showFireBaseLogin() {
        // Create and launch sign-in intent
        val providers = vmOnBoarding.getFirebaseAuthenticationProviders()
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setIsSmartLockEnabled(false)
                .setLogo(R.drawable.ic_launcher_background)
                .build(),
            RC_SIGN_IN
        )
    }

    private fun signInForGooglePhotos() {
        val intent = vmGooglePhotos.getSignInIntent()
        startActivityForResult(intent, RC_GOOGLE_SIGN_IN)
    }

}