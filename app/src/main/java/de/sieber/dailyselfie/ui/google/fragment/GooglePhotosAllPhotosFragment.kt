package de.sieber.dailyselfie.ui.google.fragment

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentGooglePhotosAllPhotosBinding
import de.sieber.dailyselfie.ui.common.Event
import de.sieber.dailyselfie.ui.google.paging.GooglePhotosAdapter
import kotlinx.android.synthetic.main.fragment_google_photos_all_photos.btnDownloadAll
import kotlinx.android.synthetic.main.fragment_google_photos_all_photos.rvGooglePhotos
import org.jetbrains.anko.sdk25.coroutines.onClick

class GooglePhotosAllPhotosFragment : SimpleGooglePhotosBaseFragment() {


    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentGooglePhotosAllPhotosBinding>(
                inflater,
                R.layout.fragment_google_photos_all_photos,
                container,
                false
            )
        ) {
            vmGooglePhotos = this@GooglePhotosAllPhotosFragment.vmGooglePhotos
            lifecycleOwner = this@GooglePhotosAllPhotosFragment
            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val spanCount = 3
        rvGooglePhotos.layoutManager = GridLayoutManager(context, spanCount)
        rvGooglePhotos.setHasFixedSize(true)
        val googlePhotosAdapter = GooglePhotosAdapter { googlePhoto ->
            googlePhoto.isSelected.set(googlePhoto.isSelected.get().not())
        }
        rvGooglePhotos.adapter = googlePhotosAdapter
        rvGooglePhotos.addItemDecoration(object : RecyclerView.ItemDecoration() {

            private val spacing: Int = 20

            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                val position = parent.getChildAdapterPosition(view) // item position
                val column = position % spanCount // item column

                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            }

        })
        btnDownloadAll.onClick {
            vmGooglePhotos.downloadSelectedPhotos()
        }

        vmGooglePhotos.photosModels.observe(this, Observer { photos ->
            photos?.let {
                googlePhotosAdapter.submitList(photos)
            }
        })
    }
    //endregion Fragment Lifecycle
}