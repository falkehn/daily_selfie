package de.sieber.dailyselfie.ui.extensions

import android.content.Context
import android.os.Environment
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import java.io.File

/**
 * This is used for sealed classes in conjunction with when(...). While there only ever can be one of the object
 * from inside a Sealed class - which eliminates the need for an else branch - this is only enforced at compile
 * time if the result of when is returned (e.g. = when(SealedClassObject) { ... }). Adding this parameter guarantees
 * that every branch is being checked.
 */
fun <T> T.exhaustive() = this

/**
 * Creates a new instance of a Fragment also allowing arguments to be passed
 * Usage: newInstanceOf<MyFragment>("foo" to true, "bar" to 0)
 *
 * Use Android KTX bundleOf when the project leaves alpha (same with parcelize)
 */
inline fun <reified T : Fragment> newInstanceOf(vararg params: Pair<String, Any>): T =
    T::class.java.newInstance().apply { arguments = bundleOf(*params) }

/**
 * A database will issue false/positives in case an id/value database is used to store values as whenever
 * any value is added/updated, all observers are notified but without any apparent change in value. This
 * snippet prevents this by using a MediatorLiveData object. Read more here:
 * https://medium.com/google-developers/7-pro-tips-for-room-fbadea4bfbd1 - Point 7
 */
fun <T> LiveData<T>.getDistinct(): LiveData<T> {
    val distinctLiveData = MediatorLiveData<T>()
    distinctLiveData.addSource(this, object : Observer<T> {
        private var initialized = false
        private var lastObj: T? = null
        override fun onChanged(obj: T?) {
            if (!initialized) {
                initialized = true
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            } else if ((obj == null && lastObj != null) || obj != lastObj) {
                lastObj = obj
                distinctLiveData.postValue(lastObj)
            }
        }
    })
    return distinctLiveData
}

fun getFilePath(context: Context): String = try {
    if (Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED) {
        context.getExternalFilesDir(null)?.absolutePath ?: ""
    } else {
        context.filesDir.absolutePath
    }
} catch (exception: IllegalStateException) {
    Timber.e(exception)
    context.filesDir.absolutePath
}

fun getVideoFilePath(context: Context, albumId: Long) = "${getFilePath(context)}/$albumId.mp4"

fun hasVideoForAlbum(context: Context, albumId: Long): Boolean {
    val file = File(getVideoFilePath(context, albumId))
    return file.exists()
}