package de.sieber.dailyselfie.ui.common

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import timber.log.Timber
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class FirebaseUtils {

    private fun getUserDatabase(): DatabaseReference? {
        val user = FirebaseAuth.getInstance().currentUser
        return if (user != null) {
            FirebaseDatabase.getInstance().reference
                .child("users")
                .child(user.uid)
        } else {
            Timber.e("Logged in User is null!")
            null
        }
    }

    suspend fun hasUsedTheAppBefore(): Boolean {
        lateinit var result: Continuation<Boolean>

        getUserDatabase()?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    val user = dataSnapshot.getValue(FirebaseUser::class.java)
                    result.resume(user != null)
                } catch (illegalStateException: IllegalStateException) {
                    illegalStateException.printStackTrace()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) = Unit

        })

        return suspendCoroutine { continuation -> result = continuation }
    }

    suspend fun getFirebaseUser(): FirebaseUser? {
        lateinit var result: Continuation<FirebaseUser?>

        getUserDatabase()?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                try {
                    val user = dataSnapshot.getValue(FirebaseUser::class.java)
                    result.resume(user)
                } catch (illegalStateException: IllegalStateException) {
                    illegalStateException.printStackTrace()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) = Unit

        })


        return suspendCoroutine { continuation -> result = continuation }
    }

    fun addNewUserToFirebase(notificationSettings: NotificationSettings, googlePhotosAlbum: GooglePhotosAlbum) {
        val user = FirebaseAuth.getInstance().currentUser ?: return
        val firebaseUser = FirebaseUser(
            uuid = user.uid,
            notificationSettings = notificationSettings,
            googlePhotosAlbum = listOf(googlePhotosAlbum)
        )
        getUserDatabase()?.setValue(
            firebaseUser
        ) { databaseError, _ ->
            Timber.i("addNewUserToFirebase:onComplete $firebaseUser, Error: $databaseError")
        }
    }

    suspend fun addAlbumToFirebaseUser(googlePhotosAlbum: GooglePhotosAlbum) {
        getFirebaseUser()?.let { firebaseUser ->
            val albums = firebaseUser.googlePhotosAlbum.toMutableList()
            albums.add(googlePhotosAlbum)
            firebaseUser.googlePhotosAlbum = albums
            getUserDatabase()?.setValue(firebaseUser) { databaseError, _ ->
                Timber.i("addAlbumToFirebaseUser:onComplete $firebaseUser, Error: $databaseError")
            }
        }
    }

    suspend fun deleteAlbumFromFirebaseUser(googlePhotosAlbumId: String) {
        getFirebaseUser()?.let { firebaseUser ->
            val albums = firebaseUser.googlePhotosAlbum.toMutableList()
            albums.removeIf { it.id == googlePhotosAlbumId }
            firebaseUser.googlePhotosAlbum = albums
            getUserDatabase()?.setValue(firebaseUser) { databaseError, _ ->
                Timber.i("addAlbumToFirebaseUser:onComplete $firebaseUser, Error: $databaseError")
            }
        }
    }

    fun userLoggedIn() = FirebaseAuth.getInstance().currentUser != null

    fun logout() = FirebaseAuth.getInstance().signOut()


}