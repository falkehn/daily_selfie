package de.sieber.dailyselfie.ui.video

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.databinding.FragmentVideoBinding
import de.sieber.dailyselfie.ui.common.fragment.SimpleBaseFragment
import de.sieber.dailyselfie.ui.extensions.toolbar.ToolbarModel
import kotlinx.android.synthetic.main.fragment_video.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class VideoFragment : SimpleBaseFragment() {

    private val vmVideo by viewModel<VideoViewModel>()
    private val player by lazy { ExoPlayerFactory.newSimpleInstance(context, DefaultTrackSelector()) }

    //region Fragment-Lifecycle ----------------------------------------------------------------------------------------
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        with(
            DataBindingUtil.inflate<FragmentVideoBinding>(
                inflater,
                R.layout.fragment_video,
                container,
                false
            )
        ) {
            setStatusBarColor(R.color.status_bar_color_red)
            return root
        }
    }

    override fun onStart() {
        super.onStart()
        context?.let {
            // video setup
            val albumId = vmVideo.getAlbumId()
            val uri = vmVideo.getVideoUri(albumId)

            val dataSource = DefaultDataSourceFactory(context, Util.getUserAgent(context, "exo"))
            val mediaSource = ExtractorMediaSource.Factory(dataSource)
                .createMediaSource(uri)

            player.prepare(mediaSource)
            player.addListener(object : Player.DefaultEventListener() {

                override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                    videoView.keepScreenOn =
                        !(playbackState == Player.STATE_IDLE || playbackState == Player.STATE_ENDED || !playWhenReady)
                }
            })
            videoView.player = player
        }
    }

    override fun onStop() {
        super.onStop()
        player.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        player.release()
    }
    //endregion Fragment-Lifecycle

    //region SimpleBaseFragment overrides ------------------------------------------------------------------------------------------------------------
    override fun createToolbar() =
        ToolbarModel.Builder()
            .withId(R.id.toolbarLayout)
            .withTitle(vmVideo.getAlbumName())
            .withDefaultNavigationButton()
            .withMenu(R.menu.video) { item ->
                when (item.itemId) {
                    R.id.action_share -> shareVideo()
                    R.id.action_create_new_video -> {
                        findNavController().navigate(R.id.action_videoFragment_to_createVideoFragment)
                    }
                }
                true
            }
            .build()
    //endregion SimpleBaseFragment overrides

    private fun shareVideo() {
        activity?.let {
            val albumId = vmVideo.getAlbumId()
            val uri = vmVideo.getVideoUri(albumId)

            val intent = ShareCompat.IntentBuilder.from(it)
                .addStream(uri)
                .setType("video/*")
                .intent
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(intent)
        }
    }

}