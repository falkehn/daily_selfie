package de.sieber.dailyselfie.ui.google.viewholder

import androidx.recyclerview.widget.RecyclerView
import de.sieber.dailyselfie.databinding.ItemGooglePhotoBinding
import de.sieber.dailyselfie.ui.model.GooglePhotoModel
import org.jetbrains.anko.sdk25.coroutines.onClick

class GooglePhotosViewHolder(
    private val binding: ItemGooglePhotoBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(googlePhotoModel: GooglePhotoModel, onGooglePhotoClick: (googlePhotoModel: GooglePhotoModel) -> Unit) {
        with(binding) {
            this.googlePhotoModel = googlePhotoModel
            this.executePendingBindings()
            this.ivPhoto.onClick {
                onGooglePhotoClick(googlePhotoModel)
            }
        }
    }

}