package de.sieber.dailyselfie.ui.common

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

open class ScopedViewModel(
    private val dispatcher: CoroutineDispatcher
) : ViewModel(), CoroutineScope {

    // This job is used to be able to cancel any thread/child thread when the viewmodel is destroyed
    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + dispatcher

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}