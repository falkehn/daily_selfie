package de.sieber.dailyselfie.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import de.sieber.dailyselfie.database.relation.AlbumWithSelfies

@Dao
abstract class AlbumWithSelfiesDao {

    @Query("SELECT * FROM albums")
    abstract fun getAlbumWithSelfies(): List<AlbumWithSelfies>

    @Query("SELECT * FROM albums WHERE id=:id")
    abstract fun getAlbumWithSelfies(id: Long): AlbumWithSelfies

    @Query("SELECT * FROM albums")
    abstract fun getAllAlbumsWithSelfiesAsLiveData(): LiveData<List<AlbumWithSelfies>>

    @Query("DELETE FROM albums WHERE id=:id")
    abstract fun deleteAlbumWithSelfies(id: Long)

}