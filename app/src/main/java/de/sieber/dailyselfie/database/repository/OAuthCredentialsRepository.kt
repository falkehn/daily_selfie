package de.sieber.dailyselfie.database.repository

import de.sieber.dailyselfie.database.dao.OAuthCredentialsDao
import de.sieber.dailyselfie.database.entities.OAuthCredentialsEntity

class OAuthCredentialsRepository(
    private val oAuthCredentialsDao: OAuthCredentialsDao
) {

    fun getOAuthCredentials() = oAuthCredentialsDao.getOAuthCredentials()

    fun insertOAuthCredentials(oAuthCredentialsEntity: OAuthCredentialsEntity) =
        oAuthCredentialsDao.insertAndDeletePreviousCredentials(oAuthCredentialsEntity)

    fun deleteAll() = oAuthCredentialsDao.deleteAll()

}