package de.sieber.dailyselfie.database.dao

import androidx.room.*
import de.sieber.dailyselfie.database.entities.OAuthCredentialsEntity

@Dao
abstract class OAuthCredentialsDao {

    @Query("SELECT * FROM oauth_credentials LIMIT 1")
    abstract fun getOAuthCredentials(): OAuthCredentialsEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun defaultInsertOrUpdate(oAuthCredentialsEntity: OAuthCredentialsEntity): Long

    @Transaction
    open fun insertAndDeletePreviousCredentials(oAuthCredentialsEntity: OAuthCredentialsEntity): Long {
        // delete previous voucher-entity (if already in DB)
        deleteAll()
        // create new voucher-entity
        return defaultInsertOrUpdate(oAuthCredentialsEntity)
    }

    @Query("DELETE FROM oauth_credentials")
    abstract fun deleteAll(): Int

}