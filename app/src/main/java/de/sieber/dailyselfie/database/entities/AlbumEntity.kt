package de.sieber.dailyselfie.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "albums")
data class AlbumEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var googlePhotosAlbumId: String = "",
    var name: String = "",
    val timeRange: String = ""
)