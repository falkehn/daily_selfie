package de.sieber.dailyselfie.database.entities

import android.graphics.Bitmap
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.ui.common.FileUtils
import de.sieber.dailyselfie.ui.common.FirebaseVision
import java.io.IOException
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


@Entity(
    tableName = "selfies",
    foreignKeys = [
        ForeignKey(
            entity = AlbumEntity::class,
            parentColumns = ["id"],
            childColumns = ["albumId"],
            onDelete = CASCADE
        )
    ]
)
data class SelfieEntity(

    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var albumId: Long = 0,
    val filePath: String = "",
    val creationTimeInSeconds: Long = 0,
    var googlePhotosId: String = "",

    var faceHeadEulerAngleY: Float = 0f,
    var faceLeft: Int = 0,
    var faceTop: Int = 0,
    var faceRight: Int = 0,
    var faceBottom: Int = 0,
    var hasDetectedFace: Boolean = false

) {

    suspend fun detectFace(albumRepository: AlbumRepository) {
        val bitmap = FileUtils().getBitmapFromFilePath(filePath)
        bitmap?.let {
            val face = findFaceFirebase(bitmap)
            face?.let {
                hasDetectedFace = true
                faceHeadEulerAngleY = it.headEulerAngleY
                faceBottom = it.boundingBox.bottom
                faceLeft = it.boundingBox.left
                faceRight = it.boundingBox.right
                faceTop = it.boundingBox.top
                albumRepository.updateSelfie(this)
            }
        }
    }

    private suspend fun findFaceFirebase(bitmap: Bitmap): FirebaseVisionFace? {
        try {
            lateinit var result: Continuation<FirebaseVisionFace?>

            val image = FirebaseVisionImage.fromBitmap(bitmap)
            FirebaseVision.faceDetector.detectInImage(image)
                .addOnSuccessListener { faces ->
                    val biggestFace = findFirebaseImageWithBiggestBoundary(faces)
                    result.resume(biggestFace)
                }
                .addOnFailureListener { exception ->
                    exception.printStackTrace()
                }

            return suspendCoroutine { continuation -> result = continuation }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    private fun findFirebaseImageWithBiggestBoundary(faces: List<FirebaseVisionFace>) = faces.asSequence().maxBy {
        it.boundingBox.width() * it.boundingBox.height()
    }

}