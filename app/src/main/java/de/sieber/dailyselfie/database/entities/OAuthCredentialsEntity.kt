package de.sieber.dailyselfie.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "oauth_credentials")
data class OAuthCredentialsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    val accessToken: String = "",
    val expiresIn: Long = 0,
    val idToken: String = ""
)