package de.sieber.dailyselfie.database.relation

import androidx.room.Embedded
import androidx.room.Relation
import de.sieber.dailyselfie.database.entities.AlbumEntity
import de.sieber.dailyselfie.database.entities.SelfieEntity

class AlbumWithSelfies {

    @Embedded
    var albumEntity: AlbumEntity? = null

    @Relation(parentColumn = "id", entityColumn = "albumId")
    var selfies: List<SelfieEntity>? = null

}