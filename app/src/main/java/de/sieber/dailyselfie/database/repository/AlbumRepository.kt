package de.sieber.dailyselfie.database.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PositionalDataSource
import de.sieber.dailyselfie.R
import de.sieber.dailyselfie.database.dao.AlbumDao
import de.sieber.dailyselfie.database.dao.AlbumWithSelfiesDao
import de.sieber.dailyselfie.database.dao.SelfieDao
import de.sieber.dailyselfie.database.entities.AlbumEntity
import de.sieber.dailyselfie.database.entities.SelfieEntity
import de.sieber.dailyselfie.database.relation.AlbumWithSelfies
import de.sieber.dailyselfie.ui.extensions.getTimeString
import de.sieber.dailyselfie.ui.model.SelfieHeaderItemModel
import de.sieber.dailyselfie.ui.model.SelfieItemBaseModel
import de.sieber.dailyselfie.ui.model.SelfiePhotoItemModel
import java.io.File
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class AlbumRepository(
    private val context: Context,
    private val albumDao: AlbumDao,
    private val albumWithSelfiesDao: AlbumWithSelfiesDao,
    private val selfiesDao: SelfieDao
) {

    fun addAlbum(albumEntity: AlbumEntity): Long = albumDao.insertAlbumEntity(albumEntity)

    fun updateAlbum(albumEntity: AlbumEntity) = albumDao.updateAlbumEntity(albumEntity)

    fun deleteAlbumWithSelfies(albumId: Long) = albumWithSelfiesDao.deleteAlbumWithSelfies(albumId)

    fun getAlbum(albumId: Long) = albumDao.getAlbum(albumId)

    fun getAlbumWithSelfies(albumId: Long): AlbumWithSelfies? = albumWithSelfiesDao.getAlbumWithSelfies(albumId)

    fun getSelfieEntitiesForAlbumIdAsc(albumId: Long) = selfiesDao.getSelfieEntitiesForAlbumIdAsc(albumId)

    fun addSelfie(albumId: Long, filePath: String): SelfieEntity {
        val creationTimeInSeconds = Calendar.getInstance().timeInMillis / 1000
        val selfie = SelfieEntity(
            albumId = albumId,
            filePath = filePath,
            creationTimeInSeconds = creationTimeInSeconds
        )
        selfie.id = selfiesDao.insertSelfieEntity(selfie)
        return selfie
    }

    fun addSelfie(selfieEntity: SelfieEntity) {
        selfiesDao.insertSelfieEntity(selfieEntity)
    }

    fun deleteSelfie(selfieId: Long) {
        val selfieEntity = selfiesDao.getSelfieEntity(selfieId)
        val filePath = selfieEntity.filePath
        val file = File(filePath)
        file.delete()
        selfiesDao.deleteSelfie(selfieId)
    }

    fun updateSelfie(selfieEntity: SelfieEntity) {
        selfiesDao.updateSelfieEntity(selfieEntity)
    }

    fun deleteAll() {
        selfiesDao.deleteAll()
        albumDao.deleteAll()
    }

    fun getAlbumsWithSelfies() = albumWithSelfiesDao.getAlbumWithSelfies()

    fun getAlbumsWithSelfiesAsLiveData() = albumWithSelfiesDao.getAllAlbumsWithSelfiesAsLiveData()

    fun getSelfiesOfAlbumAsLiveData(albumId: Long) = selfiesDao.getSelfieEntityForAlbumIdAsLiveData(albumId)

    fun getPagingLiveData(albumId: Long): LiveData<PagedList<SelfieItemBaseModel>> {
        val selfieModelFactory = SelfieItemDataSourceFactory(
            selfiesDao = selfiesDao,
            albumId = albumId,
            title = getAlbumTitle(albumId),
            description = getAlbumDescription(albumId)
        )

        val pagedListConfig = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .build()

        return LivePagedListBuilder<Int, SelfieItemBaseModel>(
            selfieModelFactory,
            pagedListConfig
        ).build()
    }

    private fun getMinDateOfAlbum(albumId: Long): Long = selfiesDao.getMinDateOfAlbum(albumId)

    private fun getMaxDateOfAlbum(albumId: Long): Long = selfiesDao.getMaxDateOfAlbum(albumId)

    private fun getCount(albumId: Long): Int = selfiesDao.getCount(albumId)

    private fun getAlbumTitle(albumId: Long): String {
        return getAlbumWithSelfies(albumId)?.albumEntity?.name ?: ""
    }

    private fun getAlbumDescription(albumId: Long): String {
        return if (getCount(albumId) > 2) {
            val firstSelfieDate = getMinDateOfAlbum(albumId)
            val lastSelfieDate = getMaxDateOfAlbum(albumId)
            val simpleDateFormat = SimpleDateFormat("dd.MM.yy", Locale.getDefault())
            val dateOfFirstSelfie = Date(firstSelfieDate * 1000)
            val dateOfLastSelfie = Date(lastSelfieDate * 1000)
            val count = getCount(albumId)
            "$count ${context.getString(
                R.string.selfie_album_description
            )} ${simpleDateFormat.format(
                dateOfFirstSelfie
            )} -  ${simpleDateFormat.format(dateOfLastSelfie)}"
        } else {
            ""
        }
    }

    private class SelfieItemDataSourceFactory(
        private val selfiesDao: SelfieDao,
        private val albumId: Long,
        private val title: String,
        private val description: String
    ) :
        DataSource.Factory<Int, SelfieItemBaseModel>() {

        override fun create(): DataSource<Int, SelfieItemBaseModel> {
            return SelfieItemDataSource(selfiesDao, albumId, title, description)
        }

    }

    private class SelfieItemDataSource(
        private val selfiesDao: SelfieDao,
        private val albumId: Long,
        private val title: String,
        private val description: String
    ) : PositionalDataSource<SelfieItemBaseModel>() {

        override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<SelfieItemBaseModel>) {
            val initialItems = mutableListOf<SelfieItemBaseModel>()
            initialItems.add(SelfieHeaderItemModel(title, description))
            initialItems.addAll(selfiesDao.getPagedItems(albumId, params.pageSize, 0).mapToSelfieItems())
            callback.onResult(initialItems, 0)
        }

        override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<SelfieItemBaseModel>) {
            val items = selfiesDao.getPagedItems(albumId, params.loadSize, params.startPosition).mapToSelfieItems()
            callback.onResult(items)
        }

        private fun List<SelfieEntity>.mapToSelfieItems() = map { input: SelfieEntity ->
            SelfiePhotoItemModel(
                id = input.id,
                googlePhotosId = input.googlePhotosId,
                face = null,
                dateAsString = input.creationTimeInSeconds.getTimeString(isInSeconds = true),
                filePath = input.filePath
            )
        }

    }

}