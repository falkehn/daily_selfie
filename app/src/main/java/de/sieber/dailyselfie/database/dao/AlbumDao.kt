package de.sieber.dailyselfie.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import de.sieber.dailyselfie.database.entities.AlbumEntity

@Dao
abstract class AlbumDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAlbumEntity(albumEntity: AlbumEntity): Long

    @Update
    abstract fun updateAlbumEntity(albumEntity: AlbumEntity)

    @Delete
    abstract fun deleteAlbumEntity(albumEntity: AlbumEntity)

    @Query("SELECT * FROM albums")
    abstract fun getAlbumEntities(): List<AlbumEntity>

    @Query("SELECT * FROM albums")
    abstract fun getAllAsLiveData(): LiveData<List<AlbumEntity>>

    @Query("SELECT * FROM albums WHERE id=:albumId")
    abstract fun getAlbum(albumId: Long): AlbumEntity

    @Query("DELETE FROM albums")
    abstract fun deleteAll()

}