package de.sieber.dailyselfie.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import de.sieber.dailyselfie.database.dao.AlbumDao
import de.sieber.dailyselfie.database.dao.AlbumWithSelfiesDao
import de.sieber.dailyselfie.database.dao.OAuthCredentialsDao
import de.sieber.dailyselfie.database.dao.SelfieDao
import de.sieber.dailyselfie.database.entities.AlbumEntity
import de.sieber.dailyselfie.database.entities.OAuthCredentialsEntity
import de.sieber.dailyselfie.database.entities.SelfieEntity

@Database(
    entities = [
        OAuthCredentialsEntity::class,
        AlbumEntity::class,
        SelfieEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun oAuthCredentialsDao(): OAuthCredentialsDao

    abstract fun galleryDao(): AlbumDao

    abstract fun selfieDao(): SelfieDao

    abstract fun galleryWithSelfiesDao(): AlbumWithSelfiesDao

}

fun getAppDatabase(
    context: Context,
    databaseName: String = "daily_selfie"
): AppDatabase {
    val appContext: Context = context.applicationContext
    val roomDbBuilder = Room.databaseBuilder(appContext, AppDatabase::class.java, databaseName)
    return roomDbBuilder
        .fallbackToDestructiveMigration()
        .allowMainThreadQueries()
        .build()
}
