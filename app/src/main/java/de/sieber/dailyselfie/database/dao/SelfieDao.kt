package de.sieber.dailyselfie.database.dao

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import de.sieber.dailyselfie.database.entities.SelfieEntity

@Dao
abstract class SelfieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertSelfieEntity(selfieEntity: SelfieEntity): Long

    @Update
    abstract fun updateSelfieEntity(selfieEntity: SelfieEntity)

    @Query("DELETE FROM selfies WHERE id = :id")
    abstract fun deleteSelfieEntity(id: Long)

    @Query(
        """
        SELECT COUNT(1) FROM selfies
        WHERE albumId=:albumId
        """
    )
    abstract fun getCount(albumId: Long): Int

    @Query("SELECT * FROM selfies WHERE id=:id")
    abstract fun getSelfieEntity(id: Long): SelfieEntity

    @Query("SELECT * FROM selfies")
    abstract fun getSelfieEntities(): List<SelfieEntity>

    @Query("SELECT * FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC")
    abstract fun getSelfieEntityForAlbumIdAsLiveData(albumId: Long): LiveData<List<SelfieEntity>>

    @Query("SELECT * FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC")
    abstract fun getSelfieEntitiesForAlbumId(albumId: Long): List<SelfieEntity>

    @Query("SELECT * FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds ASC")
    abstract fun getSelfieEntitiesForAlbumIdAsc(albumId: Long): List<SelfieEntity>

    @Query("SELECT * FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC")
    abstract fun getAllPaged(albumId: Long): DataSource.Factory<Int, SelfieEntity>

    @Query("SELECT * from selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC LIMIT :limit OFFSET :offset")
    abstract fun getPagedItems(albumId: Long, limit: Int, offset: Int): List<SelfieEntity>

    @Query("SELECT MIN(creationTimeInSeconds) FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC")
    abstract fun getMinDateOfAlbum(albumId: Long): Long

    @Query("SELECT MAX(creationTimeInSeconds) FROM selfies WHERE albumId=:albumId ORDER BY creationTimeInSeconds DESC")
    abstract fun getMaxDateOfAlbum(albumId: Long): Long

    @Query("DELETE FROM selfies WHERE id=:id")
    abstract fun deleteSelfie(id: Long)

    @Query("DELETE FROM selfies")
    abstract fun deleteAll()

}