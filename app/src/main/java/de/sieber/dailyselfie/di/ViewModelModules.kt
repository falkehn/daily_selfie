package de.sieber.dailyselfie.di

import de.sieber.dailyselfie.ui.facetimelapse.CreateVideoViewModel
import de.sieber.dailyselfie.ui.galleries.AlbumViewModel
import de.sieber.dailyselfie.ui.google.viewmodel.GooglePhotosViewModel
import de.sieber.dailyselfie.ui.onboarding.OnBoardingViewModel
import de.sieber.dailyselfie.ui.preferences.viewmodel.PrefsViewModel
import de.sieber.dailyselfie.ui.selfies.SelfiesViewModel
import de.sieber.dailyselfie.ui.video.VideoViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

@JvmField
val viewModelModules = module {

    viewModel {
        CreateVideoViewModel(
            application = androidApplication(),
            albumRepository = get()
        )
    }

    viewModel {
        GooglePhotosViewModel(
            application = androidApplication(),
            oAuthCredentialsRepository = get(),
            selfieDao = get(),
            albumRepository = get()
        )
    }
    viewModel {
        AlbumViewModel(
            preferencesHelper = get(),
            albumRepository = get()
        )
    }
    viewModel {
        SelfiesViewModel(
            application = androidApplication(),
            albumRepository = get()
        )
    }
    viewModel {
        VideoViewModel(
            application = androidApplication(),
            albumRepository = get()
        )
    }

    viewModel {
        OnBoardingViewModel(
            application = androidApplication(),
            albumRepository = get(),
            googlePhotosRepository = get(),
            notificationUtils = get()
        )
    }
    viewModel {
        PrefsViewModel(
            preferencesHelper = get(),
            notifcationUtils = get()
        )
    }
}