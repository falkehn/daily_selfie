package de.sieber.dailyselfie.di

import de.sieber.dailyselfie.database.AppDatabase
import de.sieber.dailyselfie.database.getAppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

@JvmField
val databaseModules = module {

    single {
        getAppDatabase(
            context = androidContext()
        )
    }

    single {
        get<AppDatabase>().oAuthCredentialsDao()
    }

    single {
        get<AppDatabase>().galleryDao()
    }

    single {
        get<AppDatabase>().galleryWithSelfiesDao()
    }

    single {
        get<AppDatabase>().selfieDao()
    }

}