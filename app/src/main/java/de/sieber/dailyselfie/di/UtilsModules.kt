package de.sieber.dailyselfie.di

import de.sieber.dailyselfie.ui.common.NotificationUtils
import de.sieber.dailyselfie.ui.common.PreferencesHelper
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

@JvmField
val utilsModules = module {

    single {
        PreferencesHelper(
            context = androidApplication()
        )
    }
    single {
        NotificationUtils(
            context = androidApplication()
        )
    }

}