package de.sieber.dailyselfie.di

import com.google.api.gax.core.FixedCredentialsProvider
import com.google.photos.library.v1.PhotosLibraryClient
import com.google.photos.library.v1.PhotosLibrarySettings
import de.sieber.dailyselfie.ui.google.auth.PhotosOAuth2Credentials
import de.sieber.dailyselfie.database.repository.AlbumRepository
import de.sieber.dailyselfie.database.repository.OAuthCredentialsRepository
import de.sieber.dailyselfie.ui.google.paging.GooglePhotosRepository
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

@JvmField
val repositoryModules = module {

    single {
        OAuthCredentialsRepository(
            oAuthCredentialsDao = get()
        )
    }

    single {
        AlbumRepository(
            context = androidApplication(),
            albumDao = get(),
            albumWithSelfiesDao = get(),
            selfiesDao = get()
        )
    }

    single {
        GooglePhotosRepository(
            context = androidApplication(),
            selfieDao = get(),
            googlePhotosLibraryClient = createGooglePhotosLibraryClient(
                oAuthCredentialsRepository = get()
            ),
            albumRepository = get()
        )
    }
}

private fun createGooglePhotosLibraryClient(oAuthCredentialsRepository: OAuthCredentialsRepository): PhotosLibraryClient {
    val credentials = PhotosOAuth2Credentials(oAuthCredentialsRepository)

    val settings = PhotosLibrarySettings.newBuilder()
        .setCredentialsProvider(
            FixedCredentialsProvider.create(
                credentials
            )
        )
        .build()

    return PhotosLibraryClient.initialize(settings)
}